msgid ""
msgstr ""
"Project-Id-Version: RFQ-ToolKit for WooCommerce\n"
"POT-Creation-Date: 2017-12-15 00:47-0500\n"
"PO-Revision-Date: 2017-12-15 00:47-0500\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 2.0.4\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: woo-rfq-for-woocommerce.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: includes/classes/admin/metaboxes/gpls_woo_rfq_ninja_product_meta.php:33
msgid "Enable a Ninja Form pop-up for this product."
msgstr ""

#: includes/classes/admin/metaboxes/gpls_woo_rfq_ninja_product_meta.php:36
msgid ""
"This overrides your Ninja Form pop-up setting in your RFQ-ToolKit setting "
"tab in WooCommerce Setting "
msgstr ""

#: includes/classes/admin/metaboxes/gpls_woo_rfq_ninja_product_meta.php:43
msgid "Label for Ninja Form pop-up for this product"
msgstr ""

#: includes/classes/admin/metaboxes/gpls_woo_rfq_product_meta.php:33
msgid "Enable RFQ for this product."
msgstr ""

#: includes/classes/admin/metaboxes/gpls_woo_rfq_product_meta.php:36
msgid "Enable quote requests for this product."
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:45
msgid "General"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:46
msgid "Labels"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:47
msgid "Links"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:48
msgid "Quote Request Page"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:137
msgid "RFQ-ToolKit General Options"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:139
msgid "RFQ-ToolKit general options "
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:143
msgid "Checkout Option"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:146
msgid "Normal Checkout"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:147
#: includes/classes/gateway/wc-gateway-gpls-request-quote.php:122
msgid "RFQ"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:148
msgid ""
"Normal Checkout: If normal checkout, prices will be shown except for "
"selected products (managed in product setup-advanced tab) and customer can "
"only inquire about the products that you specify in product setup in the "
"advanced tab.<br />\n"
"<br>RFQ Checkout: In RFQ mode the plugin is integrated with the WooCommerce "
"cart and the entire cart is submitted as a quote request.<br /> All the "
"prices are hidden and at checkout the option is to submit a quote request."
"<br />\n"
" \n"
"                          "
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:157
msgid "Always Show Prices With RFQ Checkout"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:159
msgid ""
"Applicable to RFQ checkout option only.<br /> Prices are shown on the site "
"but checkout is still a request for quote"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:165
msgid "Show Prices With Normal Checkout"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:167
msgid ""
"Applicable to normal checkout option only.<br /> Prices are shown on the "
"site but customer can inquire or checkout with selected products.< br />\n"
"                            This allows customers to checkout using the "
"published prices or to request a personlized quote"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:175
msgid "Hide Prices from Visitors"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:177
msgid ""
"Hide Prices From Visitor. Visitors who are not logged in can only submit a "
"quotes request.<br />.Enable guest checkout so the customers can submit "
"requests as guest"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:199
msgid " provides many more customization options and features."
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:199
msgid ""
"<strong>If RFQ-ToolKit is helpful to you, a <a target=\"_blank\" href="
"\"https://wordpress.org/support/plugin/woo-rfq-for-woocommerce/reviews/"
"\">rating</a> would be appreciated. Thank you in advance.<strong><br /><br /"
"><b>Please follow us on Twitter for news on features & updates <a target="
"\"_blank\" href=\"https://twitter.com/@NeahPlugins\" class=\"twitter-follow-"
"button\" data-show-count=\"true\">@NeahPlugins</a> and visit us at <a target="
"\"_blank\" href=\"http://www.neahplugins.com/\">NeahPlugins</a>. You can <a  "
"target=\"blank\" href=\"mailto:contact@neahplugins.com\" >contact</a> us for "
"feedback or suggestions.</b>"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:217
msgid "Custom Labels"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:219
msgid "Manage labels and wordings"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:225
msgid "Add To Cart Label"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:227
msgid ""
"Choose the text for \"Add to Cart\" - (Change to Add To Quote in RFQ "
"Checkout)"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:228
#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:237
#: includes/classes/cart/gpls_woo_rfq_cart.php:1116
msgid "Add to Cart"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:234
msgid "Add To Cart Again Label"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:236
msgid ""
"Choose the text for \"Already in the Cart\"- (Change to Add To Quote in RFQ "
"Checkout) "
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:243
msgid "Add To Quote Request Label"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:245
msgid "Normal Checkout Only. Choose the text for \"Request Quote\""
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:246
#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:255
#: includes/classes/cart/gpls_woo_rfq_cart.php:323
#: includes/classes/cart/gpls_woo_rfq_cart.php:663
#: includes/classes/cart/gpls_woo_rfq_cart.php:667
msgid "Add To Quote"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:252
msgid "Add To Quote Request Again Label"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:254
msgid "Normal Checkout Only. Choose the text for \"Already In Quote Request\""
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:261
msgid "RFQ Checkout- Proceed To Submit Your Quote Label"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:263
msgid "RFQ Checkout- Choose the text for \"Proceed To Submit Your Quote\""
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:264
#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:274
msgid "Proceed To Submit Your Quote"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:271
msgid "Normal & RFQ Checkout- Submit Your Quote Label"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:273
msgid "Normal Checkout- Choose the text for \"Submit Your Request For Quote\""
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:281
msgid "VIEW LIST"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:283
msgid "Choose the text for \"View Your Quote Cart\""
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:284
#: includes/classes/cart/gpls_woo_rfq_cart.php:407
#: includes/classes/cart/gpls_woo_rfq_cart.php:674
#: includes/classes/gpls_woo_rfq_functions.php:667
#: includes/classes/gpls_woo_rfq_functions.php:698
#: includes/classes/gpls_woo_rfq_functions.php:2612
msgid "View List"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:290
msgid "Quote Request List Is Empty Label"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:292
msgid "Choose the text for \"You Quote cart is empty and was not submitted\""
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:293
msgid "You quote request list is empty and was not submitted"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:298
msgid "Return To Shop In Quote Request Page"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:300
msgid "Choose the text for \"Return To Shop In Quote Request Page\""
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:301
#: woocommerce/woo-rfq/rfq-cart-empty.php:34
msgid "Return To Shop"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:306
msgid "Product Was Added To The Quote Request"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:308
msgid "Choose the text for \"Added to Quote List\""
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:309
msgid "Product was successfully added to quote request."
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:314
msgid "Your Request Has Been Submitted"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:316
#: includes/classes/checkout/gpls_woo_rfq_checkout.php:144
#: includes/classes/gpls_woo_rfq_functions.php:1281
msgid "Your quote request has been successfuly submitted!"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:317
msgid "Your request has been successfuly submitted!"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:322
msgid "Your Quote Request List Is Currently Empty"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:325
#: woocommerce/woo-rfq/rfq-cart-empty.php:22
msgid "Your Quote Request List is Currently Empty."
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:330
#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:333
#: woocommerce/woo-rfq/rfq-cart.php:220
msgid "Update Quote Request"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:338
#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:341
#: woocommerce/woo-rfq/rfq-cart.php:262
msgid "Customer Information"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:346
#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:349
#: includes/classes/cart/gpls_woo_rfq_cart.php:250
#: includes/classes/cart/gpls_woo_rfq_cart.php:927
#: includes/classes/cart/gpls_woo_rfq_cart.php:959
#: includes/classes/cart/gpls_woo_rfq_cart.php:997
#: includes/classes/cart/gpls_woo_rfq_cart.php:1027
msgid "Read more"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:354
msgid "Select Options"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:357
#: includes/classes/cart/gpls_woo_rfq_cart.php:361
#: includes/classes/cart/gpls_woo_rfq_cart.php:374
#: includes/classes/cart/gpls_woo_rfq_cart.php:388
msgid "Select options"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:377
msgid "Show links to Quote Request Page"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:379
msgid "Manage links to \"Request Quote Page\" - (normal checkout)"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:383
msgid "Show Link To Quote Request Page At The Top of the Product Archives Page"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:390
msgid ""
"Show Link To Quote Request Page At The Bottom of the Product Archives Page"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:397
msgid "Show Link To Quote Request Page in the Normal Cart Page"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:406
msgid "Show Link To Quote Request Page in the Single Product Description"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:414
msgid "Show Link To Quote Request Page in the Thank you page"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:433
msgid "Default Request for Quote Page: Applicable In Normal Checkout Mode"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:435
msgid ""
"<p>In the Normal Checkout mode, a default page called <i>Quote Request</i> "
"is created to view the RFQ list. This page\n"
"                                            is only needed if in the Normal "
"Checkout mode. (In RFQ mode, the WooCommerce cart\n"
"                                           is used to display the items "
"requested for quote.)</p></b> Depending on your theme, you may need to "
"manually add the <i>Quote Request</i> page to the menu. \n"
"                                           You can modify to the <i>Quote "
"Request</i> page by using the template in \"plugins/woo-rfq-for-woocommerce/"
"woocommerce/woo-rfq/rfq-cart.php\".\n"
"                                           Copy the folder to the "
"WooCommerce directory in your theme directory and modify it if you wish ."
"<br /><br />\n"
"                                           You can also use the short code "
"<b>[gpls_woo_rfq_get_cart_sc]</b>  in your own page.</p>\n"
"                                           You can control the layout and "
"the page template for this page in your WordPress admin "
"area.                                            \n"
"                                            <i> Typically the <i>Quote "
"Request</i> page looks better with a full-width template without any side-"
"bars.</i>"
msgstr ""

#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:449
#: includes/classes/admin/settings/gpls_woo_rfq_settings.php:451
msgid "Request for Quote Page URL"
msgstr ""

#: includes/classes/cart/gpls_woo_rfq_cart.php:319
msgid "In Quote List"
msgstr ""

#: includes/classes/cart/gpls_woo_rfq_cart.php:1114
msgid "Add to Quote"
msgstr ""

#: includes/classes/checkout/gpls_woo_rfq_checkout.php:87
msgid "required"
msgstr ""

#: includes/classes/checkout/gpls_woo_rfq_checkout.php:105
msgid "Update country"
msgstr ""

#: includes/classes/checkout/gpls_woo_rfq_checkout.php:167
msgid "Submit Your Request For Quote"
msgstr ""

#: includes/classes/checkout/gpls_woo_rfq_checkout.php:172
msgid "Submit Your Order"
msgstr ""

#: includes/classes/emails/class-wc-email-customer-rfq.php:23
msgid "RFQ-ToolKit New Request for Quote"
msgstr ""

#: includes/classes/emails/class-wc-email-customer-rfq.php:24
msgid ""
"This is an quote request notification sent to customers containing their "
"order details after quote request."
msgstr ""

#: includes/classes/emails/class-wc-email-customer-rfq.php:26
msgid "Thank you for your quote request"
msgstr ""

#: includes/classes/emails/class-wc-email-customer-rfq.php:27
msgid "Your {site_title} quote request confirmation from {order_date}"
msgstr ""

#: includes/classes/emails/class-wc-email-new-rfq.php:25
msgid "RFQ-ToolKit New RFQ Admin"
msgstr ""

#: includes/classes/emails/class-wc-email-new-rfq.php:26
msgid ""
"New quote request emails are sent to the recipient list when an order is "
"received."
msgstr ""

#: includes/classes/emails/class-wc-email-new-rfq.php:28
msgid "New customer quote request"
msgstr ""

#: includes/classes/emails/class-wc-email-new-rfq.php:29
msgid ""
"[{site_title}] New customer quote request ({order_number}) - {order_date}"
msgstr ""

#: includes/classes/emails/class-wc-email-new-rfq.php:131
#: includes/classes/gateway/wc-gateway-gpls-request-quote.php:57
msgid "Enable/Disable"
msgstr ""

#: includes/classes/emails/class-wc-email-new-rfq.php:133
msgid "Enable this email notification"
msgstr ""

#: includes/classes/emails/class-wc-email-new-rfq.php:137
msgid "Recipient(s)"
msgstr ""

#: includes/classes/emails/class-wc-email-new-rfq.php:139
#, php-format
msgid ""
"Enter recipients (comma separated) for this email. Defaults to <code>%s</"
"code>."
msgstr ""

#: includes/classes/emails/class-wc-email-new-rfq.php:144
msgid "Subject"
msgstr ""

#: includes/classes/emails/class-wc-email-new-rfq.php:146
#, php-format
msgid ""
"This controls the email subject line. Leave blank to use the default "
"subject: <code>%s</code>."
msgstr ""

#: includes/classes/emails/class-wc-email-new-rfq.php:151
msgid "Email Heading"
msgstr ""

#: includes/classes/emails/class-wc-email-new-rfq.php:153
#, php-format
msgid ""
"This controls the main heading contained within the email notification. "
"Leave blank to use the default heading: <code>%s</code>."
msgstr ""

#: includes/classes/emails/class-wc-email-new-rfq.php:158
msgid "Email type"
msgstr ""

#: includes/classes/emails/class-wc-email-new-rfq.php:160
msgid "Choose which format of email to send."
msgstr ""

#: includes/classes/emails/class-wc-email-rfq.php:48
msgid "Note"
msgstr ""

#: includes/classes/gateway/wc-gateway-gpls-request-quote.php:29
#: woo-rfq-for-woocommerce.php:250 woo-rfq-for-woocommerce.php:342
msgid "Quote Request"
msgstr ""

#: includes/classes/gateway/wc-gateway-gpls-request-quote.php:30
msgid "Allows RFQ to go through."
msgstr ""

#: includes/classes/gateway/wc-gateway-gpls-request-quote.php:59
msgid "Skip Payment For Quote Requests"
msgstr ""

#: includes/classes/gateway/wc-gateway-gpls-request-quote.php:63
msgid "Title"
msgstr ""

#: includes/classes/gateway/wc-gateway-gpls-request-quote.php:65
msgid "This controls the title which the user sees during checkout."
msgstr ""

#: includes/classes/gateway/wc-gateway-gpls-request-quote.php:66
msgid "Request For Quote"
msgstr ""

#: includes/classes/gateway/wc-gateway-gpls-request-quote.php:70
msgid "Description"
msgstr ""

#: includes/classes/gateway/wc-gateway-gpls-request-quote.php:72
msgid "Payment method description that the customer will see on your checkout."
msgstr ""

#: includes/classes/gateway/wc-gateway-gpls-request-quote.php:77
msgid "Instructions"
msgstr ""

#: includes/classes/gateway/wc-gateway-gpls-request-quote.php:79
msgid "Instructions that will be added to the thank you page and emails."
msgstr ""

#: includes/classes/gpls_woo_rfq_functions.php:116
msgid "View cart"
msgstr ""

#: includes/classes/gpls_woo_rfq_functions.php:700
msgid "You have items in your Request for Quote Cart"
msgstr ""

#: includes/classes/gpls_woo_rfq_functions.php:1072
#: includes/classes/gpls_woo_rfq_functions.php:1108
#: includes/classes/gpls_woo_rfq_functions.php:1382
#: includes/classes/gpls_woo_rfq_functions.php:1428
msgid "You RFQ cart is empty and was not submitted"
msgstr ""

#: includes/classes/gpls_woo_rfq_functions.php:1334
msgid "Please enter first name, last name and a valid email"
msgstr ""

#: includes/classes/gpls_woo_rfq_functions.php:1346
msgid "Invalid email address."
msgstr ""

#: includes/classes/gpls_woo_rfq_functions.php:1356
msgid "Invalid phone number."
msgstr ""

#: includes/classes/gpls_woo_rfq_functions.php:1636
msgid "You RFQ has been successfuly submitted!"
msgstr ""

#: includes/classes/gpls_woo_rfq_functions.php:2455
msgid ""
"Sorry, this product cannot be purchased and has been removed from the cart"
msgstr ""

#: includes/classes/gpls_woo_rfq_functions.php:2701
msgid "Create an account?"
msgstr ""

#: woo-rfq-for-woocommerce.php:257
#, php-format
msgid "<span class=\"count\">(%s)</span>"
msgid_plural " <span class=\"count\">(%s)</span>"
msgstr[0] ""

#: woocommerce/cart/proceed-to-checkout-button.php:43
msgid "Proceed To Submit Your RFQ"
msgstr ""

#: woocommerce/cart/proceed-to-checkout-button.php:51
msgid "Proceed to checkout"
msgstr ""

#: woocommerce/emails/admin-new-rfq.php:20
#, php-format
msgid ""
"You have received an request for a quote from %s. The request is as follows:"
msgstr ""

#: woocommerce/emails/admin-new-rfq.php:24
#: woocommerce/emails/customer-note.php:45
#: woocommerce/emails/customer-rfq.php:36
#: woocommerce/emails/plain/customer-note.php:38
#, php-format
msgid "Order #%s"
msgstr ""

#: woocommerce/emails/admin-new-rfq.php:30
#: woocommerce/emails/customer-note.php:51
#: woocommerce/emails/customer-rfq.php:42
#: woocommerce/emails/plain/customer-note.php:44
#: woocommerce/woo-rfq/rfq-cart.php:84 woocommerce/woo-rfq/rfq-cart.php:141
msgid "Product"
msgstr ""

#: woocommerce/emails/admin-new-rfq.php:31
#: woocommerce/emails/customer-note.php:52
#: woocommerce/emails/customer-rfq.php:43
#: woocommerce/emails/plain/customer-note.php:45
#: woocommerce/woo-rfq/rfq-cart.php:85 woocommerce/woo-rfq/rfq-cart.php:165
msgid "Quantity"
msgstr ""

#: woocommerce/emails/admin-new-rfq.php:32
#: woocommerce/emails/customer-note.php:55
#: woocommerce/emails/customer-rfq.php:46
#: woocommerce/emails/plain/customer-note.php:48
msgid "Price"
msgstr ""

#: woocommerce/emails/customer-note.php:40
#: woocommerce/emails/plain/customer-note.php:33
msgid "Hello, a note has just been added to your order:"
msgstr ""

#: woocommerce/emails/customer-note.php:44
#: woocommerce/emails/plain/customer-note.php:37
msgid "For your reference, your order details are shown below."
msgstr ""

#: woocommerce/emails/customer-rfq.php:32
#: woocommerce/emails/plain/customer-rfq.php:16
msgid ""
"Your request has been received and is now being reviewed. Your request "
"details are shown below for your reference:"
msgstr ""

#: woocommerce/emails/email-order-items.php:46
msgid "Product image"
msgstr ""

#: woocommerce/emails/plain/admin-new-rfq.php:16
#, php-format
msgid "You have received a request for a quote from %s."
msgstr ""

#: woocommerce/emails/plain/admin-new-rfq.php:22
#: woocommerce/emails/plain/customer-rfq.php:22
#, php-format
msgid "Order number: %s"
msgstr ""

#: woocommerce/emails/plain/admin-new-rfq.php:23
#: woocommerce/emails/plain/customer-rfq.php:23
msgid "jS F Y"
msgstr ""

#: woocommerce/emails/plain/admin-new-rfq.php:38
#, php-format
msgid "View order: %s"
msgstr ""

#: woocommerce/emails/plain/email-order-items.php:45
#, php-format
msgid "Quantity: %s"
msgstr ""

#: woocommerce/emails/plain/email-order-items.php:58
#, php-format
msgid "Download %d"
msgstr ""

#: woocommerce/emails/plain/email-order-items.php:60
msgid "Download"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:117
msgid "Remove this item"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:158
msgid "Available on backorder"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:278
msgid "First Name"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:289
msgid "Last Name"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:301
msgid "Email"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:315
msgid "Phone"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:325
msgid "Company"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:336
msgid "Country"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:349
msgid "Required"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:366 woocommerce/woo-rfq/rfq-cart.php:375
msgid "State"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:439
msgid "State/County"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:445
msgid "Address"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:456
msgid "Address 2"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:470
msgid "City"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:482
msgid "Zip"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:510
msgid "Customer Note"
msgstr ""

#: woocommerce/woo-rfq/rfq-cart.php:516
msgid "Your message to us"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "RFQ-ToolKit For WooCommerce"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "https://wordpress.org/plugins/woo-rfq-for-woocommerce"
msgstr ""

#. Description of the plugin/theme
msgid "RFQ-ToolKit: Request For Quote For WooCommerce."
msgstr ""

#. Author of the plugin/theme
msgid "Neah Plugins"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://www.neahplugins.com/"
msgstr ""
