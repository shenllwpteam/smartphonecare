<?php
function icon_main_category_metabox_fun(){
   add_meta_box( 'icon_main_category_meta_box',
        'Icon Class',
        'display_main_category_icon_meta_box_callback',
        'form_category', 'normal', 'high'
    );
	}
function sub_category_metabox_fun(){
   add_meta_box( 'sub_category_meta_box',
        'Main Category',
        'display_sub_category_meta_box_callback',
        'form_sub_category', 'normal', 'high'
    );
	}
function price_category_metabox_fun(){
   add_meta_box( 'price_category_meta_box',
        'Price',
        'display_sub_category_price_meta_box_callback',
        'form_sub_category', 'normal', 'high'
    );
	}
function display_sub_category_boxstyle_meta_box_fun() {
	 add_meta_box( 'boxstyle_category_meta_box',
        'Sub Category Box Style',
        'display_sub_category_boxstyle_meta_box_callback',
        'form_sub_category', 'normal', 'high'
    );
	}
function display_price_show_boxstyle_meta_box_fun() {
	 add_meta_box( 'price_show_meta_box',
        'Price Show',
        'display_price_show_boxstyle_meta_box_callback',
        'form_sub_category', 'normal', 'high'
    );
	}	
function icon_category_metabox_fun(){
   add_meta_box( 'icon_category_meta_box',
        'Icon Class',
        'display_sub_category_icon_meta_box_callback',
        'form_sub_category', 'normal', 'high'
    );
	}
function details_category_metabox_fun(){
  add_meta_box( 'details_category_meta_box',
        'Details',
        'display_sub_category_details_meta_box_callback',
        'form_sub_category', 'normal', 'high'
    );
	}
function option_category_price_metabox_fun(){
   add_meta_box( 'price_option_meta_box',
        'Price',
        'display_option_category_price_meta_box_callback',
        'form_options', 'normal', 'high'
    );
	}
function option_sub_category_metabox_fun(){
   add_meta_box( 'sub_category_meta_box1',
        'Sub Category',
        'display_sub_category_options_meta_box_callback',
        'form_options', 'normal', 'high'
    );
	}
function display_main_category_icon_meta_box_callback() {
     global $post;
	 $post_id = $post->ID;
     echo '<input type="text" id="main_cat_icon" name="main_cat_icon" value="'.get_post_meta($post_id, 'main_cat_icon',true).'"/>';
}
function display_sub_category_meta_box_callback() {
	global $post;
    $tmp_post = $post;
	$args = array(
		'post_type' => 'form_category',
		'nopaging'  => true,
		'post_status'  => 'publish',
	);
	$query = new WP_Query( $args );
	echo '<select name="category_metabox">';
	while ( $query->have_posts() ) : $query->the_post();
		$selected = get_post_meta($tmp_post->ID, 'category_metabox',true);
		echo '<option value="'.$post->ID.'"';
		if ($post->ID == $selected)
			echo ' selected="selected"';
		echo '>';
		the_title();
		echo '</option>';
	endwhile;	 
	$post = $tmp_post; 
	echo '</select>';
}
function display_sub_category_options_meta_box_callback() {
	global $post;
    $tmp_post = $post;
	$args = array(
		'post_type' => 'form_sub_category',
		'nopaging'  => true,
		'post_status'  => 'publish',
	);
	$query = new WP_Query( $args );
	echo '<select name="category_options_metabox">';
	while ( $query->have_posts() ) : $query->the_post();
	echo $tmp_post->ID;
		$selected = get_post_meta($tmp_post->ID, 'category_options_metabox',true);
		echo $selected;
		echo '<option value="'.$post->ID.'"';
		if ($post->ID == $selected)
			echo ' selected="selected"';
		echo '>';
		the_title();
		echo '</option>';
	endwhile;	 
	$post = $tmp_post; 
	echo '</select>';
}
function display_sub_category_price_meta_box_callback() {
     global $post;
	 $post_id = $post->ID;
     echo '<input type="text" id="sub_cat_price" name="sub_cat_price" value="'.get_post_meta($post_id, 'sub_cat_price',true).'"/>';
}
function display_sub_category_boxstyle_meta_box_callback() {
     global $post;
	 $tmp_post = $post;
	 $post_id = $post->ID;
	 echo '<select name="sub_cat_boxstyle">';
	 echo $tmp_post->ID;
		$selected = get_post_meta($tmp_post->ID, 'sub_cat_boxstyle',true);
		echo $selected;
	 echo '<option value="1"';
		if (1 == $selected)
			echo ' selected="selected"';
		echo '>';
		echo ''.__(" Package Type  ", 'order-now' ).'';
		echo '</option>';
	 echo '<option value="2"';
		if (2 == $selected)
			echo ' selected="selected"';
		echo '>';
		echo ''.__(" Item Type  ", 'order-now' ).'';
		echo '</option>';
		echo '</select>';
}
function display_price_show_boxstyle_meta_box_callback() {
     global $post;
	 $tmp_post = $post;
	 $post_id = $post->ID;
	 echo '<select name="sub_cat_boxstyle">';
	 echo $tmp_post->ID;
		$selected = get_post_meta($tmp_post->ID, 'sub_cat_boxstyle',true);
		echo $selected;
	 echo '<option value="1"';
		if (1 == $selected)
			echo ' selected="selected"';
		echo '>';
		echo ''.__(" Package Type  ", 'order-now' ).'';
		echo '</option>';
	 echo '<option value="2"';
		if (2 == $selected)
			echo ' selected="selected"';
		echo '>';
		echo ''.__(" Item Type  ", 'order-now' ).'';
		echo '</option>';
		echo '</select>';
}
function display_sub_category_icon_meta_box_callback() {
     global $post;
	 $post_id = $post->ID;
     echo '<input type="text" id="sub_cat_icon" name="sub_cat_icon" value="'.get_post_meta($post_id, 'sub_cat_icon',true).'"/>';
}
function display_sub_category_details_meta_box_callback() {
     global $post;
	 $post_id = $post->ID;
	 	   $field_value = get_post_meta( $post->ID, 'sub_cat_details', true );
           wp_editor( $field_value, 'sub_cat_details' );
}
//For option category
function display_option_category_price_meta_box_callback() {
     global $post;
	 $post_id = $post->ID;
     echo '<input type="text" id="option_cat_price" name="option_cat_price" value="'.get_post_meta($post_id, 'option_cat_price',true).'"/>';
}
?>