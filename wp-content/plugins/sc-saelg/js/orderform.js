(function($) {

	"use strict";
/*! Script for form price calculation */
	var order_total = 0;
    /* default values goes here*/
	var order_type_cost = 0;
	var order_additional_options = 0;
	var conversion_inner_page = 0;
	var conversion_inner_page_cost = 0;
	/* default coupon code */
	var coupon_code;
	var discount = 0;
    var coupon_per = "";
	var allVals = new Array();	
	var item_quantity=1;	
	$(document).ready(function(e)
	{
		var sect_width = $(".reo-order-form").width();
		if(sect_width<=900)
		{
			$('.reo-order-form').attr('id', 'reo-section-id') 
		}
		$('#reo-order-summary-box').hide();
		$.ajax({
			url: ajaxurl,
			data: {
				'action':'get_first_category_ajax_request',
		},
		success:function(response) {
			if(response!="")
			{
				var res = response.split('||');
				$('#summary-pack h5').html(res[0]);
				$('#reo-result').html(res[1]);
				if(res[1]!="")
				{
				 $('#reo-list-opt').html('Please select a package to choose for additional options');
				 $('#reo-order-summary-box').show();
				 $('#reo-order-concept-area').hide();
				}
				else
				{   
					 $('#reo-order-summary-box').hide();
					 $('#reo-order-concept-area').show();
				}
				$('.cms-cont').removeClass('active');
				$('.normal-pk').removeClass('active');
			}
		},
		error: function(errorThrown){
			console.log(errorThrown);
		}
		});	 
		order_type_cost = 0;
		$('input[name=type_cost]:text').val(order_type_cost);	
		$('#reo-order-summary-box').hide();
		/* update the cart */
		cart_update();
	});
	function cart_update()
	{
		/* calculate the total */
	    order_total=(Number(order_type_cost)+Number(order_additional_options)+Number(conversion_inner_page_cost))*Number(item_quantity);	
		if(discount==1)
		{
			order_total = order_total - (order_total*coupon_per/100);
		}
		$.ajax({
			url: ajaxurl,
			data: {
				'action':'get_currency_ajax_request',
		},
		success:function(response) {
			  var currency = response;
			  $('#order_total').html('<span>'+response+'</span>'+(order_total));
		},
		error: function(errorThrown){
			console.log(errorThrown);
		}
		});	 
		/* Assign the total amount to a text field for form submission */
		$('input[name=order_total_amt]:text').val(order_total);
	  }
	$(document).delegate('.reo-order-opt-link', 'click', function()  
	{	
		// We'll pass this variable to the PHP function example_ajax_request
		var category = $( this ).attr('data-param1');
		var category_title = $( this ).attr('data-param2');
		$('input[name=customer_choice]:text').val(category_title);
		$('input[name=customer_sub_choice]:text').val('');
		$(".reo-order-opt-link").removeClass('active');
		$(this).addClass('active');
		$('#reo-add_options').removeClass('reo-addi-opts');
		$('#reo-list-opt').html('');
		$('#pack-add').html('');
		$('#reo-pack-in').html('');
		$('#summary-pack h5').html(category_title);
		order_type_cost = 0;
		allVals.length=0
		$('#reo-result').html("Loading...");
	   // This does the ajax request
		$.ajax({
		url: ajaxurl,
		data: {
			'action':'category_ajax_request',
			'category' : category
		},
		success:function(response) {
			$('#reo-result').html(response);
			if(response!="")
			{
				$('#reo-list-opt').html('Please select a package to choose your additional options');
				$('#reo-order-summary-box').show();
				$('#reo-order-concept-area').hide();
			}
			else
			{
				$('#reo-order-summary-box').hide();
				$('#reo-order-concept-area').show();
			}
		},
		error: function(errorThrown){
			console.log(errorThrown);
		}
		});	  
		order_additional_options = 0;
		conversion_inner_page = 0;
		conversion_inner_page_cost = 0;
		cart_update();
    });
	$(document).delegate('.normal-pk', 'click', function()
	{
		$('.normal-pk').removeClass('active');
		$(this).addClass('active');
		var sub_category = $( this ).attr('data-param1');
		var sub_category_title = $( this ).attr('data-param2');
		$('input[name=customer_sub_choice]:text').val(sub_category_title);	
		$('#reo-list-opt').html('');
		$('#pack-add').html('');
		$('#reo-pack-in').html('');
		allVals.length=0
    	order_additional_options =0;
		discount=0;
		$('input[name=coupon_text]:text').val('');
		$('input[name=conversion_inner_pages]:text').val(0);
		$('input[name=item_quantity]:text').val(1);
		$('#reo-list-opt').html("Loading...");
		 // This does the ajax request
		$.ajax({
		url: ajaxurl,
		data: {
			'action':'option_ajax_request',
			'sub_category' : sub_category
		},
		success:function(response) {
			if(response!="")
			{
			$('#summary-pack h5').html(sub_category_title);
			$('#reo-add_options').addClass('reo-addi-opts');
			$('#reo-list-opt').html(response);	
			}
		},
		error: function(errorThrown){
			console.log(errorThrown);
		}
		});
		$.ajax({
		url: ajaxurl,
		data: {
			'action':'sub_category_details_ajax_request',
			'sub_category' : sub_category
		},
		success:function(response) {
			var res = response.split('@');
			$('#reo-pack-in').html(res[0]);
			order_type_cost = res[1];
			$('input[name=type_cost]:text').val(order_type_cost);
			$('#dis_price').html('');
			cart_update();
		},
		error: function(errorThrown){
			console.log(errorThrown);
		}
		});	 	 
	});
	$(document).delegate('.cms-cont', 'click', function()
	{
		$('.cms-cont').removeClass('active');
		$(this).addClass('active');
		var sub_category = $( this ).attr('data-param1');
		var sub_category_title = $( this ).attr('data-param2');
		$('input[name=customer_sub_choice]:text').val(sub_category_title);	
		$('#reo-list-opt').html('');
		$('#pack-add').html('');
		$('#reo-pack-in').html('');
		$('input[name=conversion_inner_pages]:text').val(0);
		allVals.length=0
		order_additional_options =0;
		discount=0;
		$('input[name=coupon_text]:text').val('');
		$('input[name=item_quantity]:text').val(1);
		$('#reo-list-opt').html("Loading...");
    	 // This does the ajax request
		$.ajax({
		url: ajaxurl,
		data: {
			'action':'option_ajax_request',
			'sub_category' : sub_category
		},
		success:function(response) {
			$('#reo-add_options').addClass('reo-addi-opts');
			$('#reo-list-opt').html(response);
			$('#summary-pack h5').html(sub_category_title);
		},
		error: function(errorThrown){
			console.log(errorThrown);
		}
		});	 
		$.ajax({
		url: ajaxurl,
		data: {
			'action':'sub_category_details_ajax_request',
			'sub_category' : sub_category
		},
		success:function(response) {
			var res = response.split('@');
			$('#reo-pack-in').html(res[0]);
			order_type_cost = res[1];
			$('input[name=type_cost]:text').val(order_type_cost);	
			$('#dis_price').html('');
			cart_update();
		},
		error: function(errorThrown){
			console.log(errorThrown);
		}
		});	 
	});
	// code for discount coupon
	$(document).delegate('#discnt_btn_id', 'click', function()  
	{
	    var coupon_value = $('input[name=coupon_text]:text').val();
		$.ajax({
		url: ajaxurl,
		data: {
			'action':'coupon_ajax_request',
		},
		success:function(response) {
			var res = response.split('|');
			coupon_code = res[0]; //coupon code
			coupon_per = res[1]; //  coupon percentage
			var currency  = res[2];
			if(order_total==0)
			{
				$('#dis_price').html("Please select any package");
			}
			else
			{
				if((coupon_value==coupon_code) && (discount==0))
				{
					discount=1;
					//calculate discount percenatge
					var discount_amt = order_total*coupon_per/100;
					var discounted_price = order_total-discount_amt;
					// actual amount storedin a textbox
					$('input[name=actual_amt]:text').val(order_total);
					$('input[name=order_total_amt]:text').val(discounted_price);
					$('input[name=discount_text]:text').val(discount);	
					$('input[name=discount_rate_cart]:text').val(coupon_per);	
					$('#dis_price').html('<span>'+currency+'</span>'+order_total+' - '+coupon_per+'% discount ('+currency+''+discount_amt +') = '+currency+''+discounted_price);	
					// discount price display on disscount price div
				}
				else if(coupon_value==coupon_code)
				{
					// compare coupon code with user entered code
					discount=1; 
					$('input[name=discount_text]:text').val(discount);	
					$('input[name=discount_rate_cart]:text').val(coupon_per);			
					$('#dis_price').html("");
				}
				else
				{
					discount=0; 
					$('#dis_price').html('Coupon code is not valid');
					$('input[name=coupon_text]:text').val('');
				}
			}
				cart_update();
			},
			error: function(errorThrown){
				console.log(errorThrown);
			}
			});	 
		return true;
    });
	// end discount coupon code
	// add and remove check box items on summary
	$(document).delegate('.check-opt', 'click', function()
	{
		var liid ='';
		var newval='';
		var latvalue='';
		if($(this).is(':checked'))
		{
			newval = $(this).attr('data');
			order_additional_options+= Number(newval);
			liid = $(this).attr('id');
			$("#nothing").remove();
			$(".pack-add").append('<li id="res'+liid+'">'+($(this).attr('value'))+'</li>');
			allVals.push($(this).val());
		}
		else
		{   // minus unchecked value 
			newval = $(this).attr('data');
			order_additional_options-= Number(newval);
			liid = $(this).attr('id');			
		    $("#res"+liid+"").remove();
			var i = allVals.indexOf($(this).attr('value'));
			if(i != -1) {
				allVals.splice(i, 1);
			}
		}
		cart_update();
	});
	$('input[name=conversion_inner_pages]:text').on('keyup', function(e)
	{
		if(isNaN($(this).val()))
		{
			$(this).val(0);
		}
		else
		{
		conversion_inner_page = $(this).val();
		$.ajax({
		url: ajaxurl,
		data: {
			'action':'innerpage_ajax_request',
		},
		success:function(response) {
			var conversion_cost = response;
			conversion_inner_page_cost = conversion_inner_page*Number(conversion_cost);
			$('input[name=innerpage_cost]:text').val(conversion_inner_page_cost);	
			$('#dis_price').html('');		
			cart_update();
		},
		error: function(errorThrown){
			console.log(errorThrown);
		}	
		});	 
		}
	});	
	$('input[name=item_quantity]:text').on('keyup', function(e)
	{
		if(isNaN($(this).val()) || ($(this).val()==0))
		{
			$(this).val(1);
		}
		else
		{
		item_quantity = $(this).val();
		cart_update();
		}
	});
	// on click order button submit the form
	$('#order_btn_id').on('click', function(e)
	{
	 e.preventDefault();
		if($('input[name=customer_name]:text').val()=='')
		{
			$('#error_order').css('display','block').html('Please enter your name');
			$('input[name=customer_name]:text').focus();
			return false;
		}
		else if($('input[name=customer_email]:text').val()=='')
		{
			$('#error_order').css('display','block').html('Please enter your email');
			$('input[name=customer_email]:text').focus();
			return false;
		}
		else if(!validate_email($('input[name=customer_email]:text').val()))
		{
			$('#error_order').css('display','block').html('Please enter your valid email');
			$('input[name=customer_email]:text').focus();
			return false;
		}	
		else if($('textarea[name=customer_message]').val()=='')
		{
			$('#error_order').css('display','block').html('Please enter your message');
			$('textarea[name=customer_message]').focus();
			return false;
		}
		else
		{
			var customer_name = $('input[name=customer_name]:text').val();
			var customer_email = $('input[name=customer_email]:text').val();
			var customer_contact = $('input[name=customer_contact]:text').val();
			var customer_message = $('textarea[name=customer_message]').val();
			var customer_choice = $('input[name=customer_choice]').val();
			var innerpage_cost 	= $('input[name=innerpage_cost]').val();
			var item_quantity   =  $('input[name=item_quantity]').val();
			var customer_sub_choice	= $('input[name=customer_sub_choice]').val();
			var coupon_value = $('input[name=coupon_text]').val();
			var discount_text = $('input[name=discount_text]').val();
            var conversion_inner_pages = $('input[name=conversion_inner_pages]').val();
			var type_cost 			= $('input[name=type_cost]').val();
			var actual_amt 			= $('input[name=actual_amt]').val();
			var order_total_amt 		= $('input[name=order_total_amt]').val();	
			$.ajax({ 
			data: {action: 'order_form_ajax_request', name:customer_name,email:customer_email,contact:customer_contact,message:customer_message,customerchoice:customer_choice,customersubchoice:customer_sub_choice,typecost:type_cost,conversion_inner_pages:conversion_inner_pages,innerpage_cost:innerpage_cost,item_quantity:item_quantity,couponvalue:coupon_value,discount_text:discount_text,ordertotalamt:order_total_amt,actualamt:actual_amt,otheroptions:allVals},
			url: ajaxurl,
			success: function(response) {
			 $("#holderdiv").html(response);
			}
			});
			return true;
		}
	});
	// on click order button of basic form then submit the order form 
	$('#order_btn_id2').on('click', function() {
		if($('input[name=customer_name]:text').val()=='')
		{
			$('#error_order').css('display','block').html('Please enter your name');
			$('input[name=customer_name]:text').focus();
			return false;
		}
		else if($('input[name=customer_email]:text').val()=='')
		{
			$('#error_order').css('display','block').html('Please enter your email');
			$('input[name=customer_email]:text').focus();
			return false;
		}
		else if(!validate_email($('input[name=customer_email]:text').val()))
		{
			$('#error_order').css('display','block').html('Please enter your valid email');
			$('input[name=customer_email]:text').focus();
			return false;
		}	
		else if($('textarea[name=customer_message]').val()=='')
		{
			$('#error_order').css('display','block').html('Please enter your message');
			$('textarea[name=customer_message]').focus();
			return false;
		}
		else
		{
			var customer_name = $('input[name=customer_name]:text').val();
			var customer_email = $('input[name=customer_email]:text').val();
			var customer_contact = $('input[name=customer_contact]:text').val();
			var customer_message = $('textarea[name=customer_message]').val();
			var customer_choice = $('input[name=customer_choice]').val();
			var innerpage_cost = $('input[name=innerpage_cost]').val();
			var customer_sub_choice	= $('input[name=customer_sub_choice]').val();
			var discounted_cost = '';
			var coupon_value = '';
			var discount_text = '';
            var conversion_inner_pages = '';
			var item_quantity = '';
			var type_cost 			= $('input[name=type_cost]').val();
			var actual_amt 			= $('input[name=actual_amt]').val();
			var order_total_amt 	= $('input[name=order_total_amt]').val();	
			$.ajax({ 	
				data: {action: 'order_form_ajax_deliver_mail', name:customer_name,email:customer_email,contact:customer_contact,message:customer_message,customerchoice:customer_choice,customersubchoice:customer_sub_choice,typecost:type_cost,couponvalue:coupon_value,discounted_cost:discounted_cost,discount_text:discount_text,ordertotalamt:order_total_amt,actualamt:actual_amt,otheroptions:allVals,inner_pages_cost:innerpage_cost,item_quantity:item_quantity},
				url: ajaxurl,
				success: function(response) {
				 $("#holderdiv").html(response);
				}
			});
		}
	});		
	$(document).on('click', '#order-mail-id', function(e) {
	 e.preventDefault();
		var customer_name = $('input[name=customer_name]').val();
		var customer_email = $('input[name=customer_email]').val();
		var customer_contact = $('input[name=customer_contact]').val();
		var customer_message = $('input[name=customer_message]').val();
		var customer_choice = $('input[name=customer_choice]').val();
		var customer_sub_choice	= $('input[name=sub_type]').val();
		var coupon_value = $('input[name=coupon_text]').val();
		var discount_text = $('input[name=discount_text]').val();
		var conversion_inner_pages = $('input[name=conversion_inner_pages]').val();
		var type_cost 			= $('input[name=type_cost]').val();
		var discounted_cost 	= $('input[name=discounted_cost]').val();
		var order_total_amt 		= $('input[name=total_cost]').val();	
		var inner_pages_cost		= $('input[name=inner_pages_cost]').val();
		var item_quantity =  $('input[name=item_quantity]').val();
		$.ajax({ 	
			data: {action: 'order_form_ajax_deliver_mail', name:customer_name,email:customer_email,contact:customer_contact,message:customer_message,customerchoice:customer_choice,customersubchoice:customer_sub_choice,typecost:type_cost,couponvalue:coupon_value,discount_text:discount_text,ordertotalamt:order_total_amt,discounted_cost:discounted_cost,otheroptions:allVals,inner_pages_cost:inner_pages_cost,item_quantity:item_quantity},
			url: ajaxurl,
			success: function(response) {
			 $("#holderdiv").html(response);
			}
		});
	});
	$(document).on('click', '#order_pay_id', function(e) {
	 e.preventDefault();
		var customer_name = $('input[name=customer_name]').val();
		var customer_email = $('input[name=customer_email]').val();
		var customer_contact = $('input[name=customer_contact]').val();
		var customer_message = $('input[name=customer_message]').val();
		var customer_choice = $('input[name=customer_choice]').val();
		var customer_sub_choice	= $('input[name=sub_type]').val();
		var coupon_value = $('input[name=coupon_text]').val();
		var discount_text = $('input[name=discount_text]').val();
		var conversion_inner_pages = $('input[name=conversion_inner_pages]').val();
		var item_quantity = $('input[name=item_quantity]').val();
		var type_cost 			= $('input[name=type_cost]').val();
		var discounted_cost 	= $('input[name=discounted_cost]').val();
		var order_total_amt 		= $('input[name=total_cost]').val();	
		var inner_pages_cost		= $('input[name=inner_pages_cost]').val();
		var site_url =  $('input[name=site_url]').val();
		var custom="name="+customer_name+"&email="+customer_email+"&contact="+customer_contact+"&message="+customer_message+"&site_url="+site_url;
		$('#custom').val(custom);
		$.ajax({ 	
			data: {action: 'order_form_ajax_deliver_mail', name:customer_name,email:customer_email,contact:customer_contact,message:customer_message,customerchoice:customer_choice,customersubchoice:customer_sub_choice,typecost:type_cost,couponvalue:coupon_value,discount_text:discount_text,ordertotalamt:order_total_amt,discounted_cost:discounted_cost,otheroptions:allVals,inner_pages_cost:inner_pages_cost,item_quantity:item_quantity},
			url: ajaxurl,
			success: function(response) {
				$('#reo-order-pay-frm').attr('action', 'https://www.paypal.com/cgi-bin/webscr');
				$('#reo-order-pay-frm').submit();
			}
		});

		return true;
	});
	$('input:text, textarea').keyup(function(e) {
			$('#error_order').css('display','none');
		});	

})(jQuery);			
function validate_email(email) 
{
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
   return reg.test(email);
}