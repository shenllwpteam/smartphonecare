<?php
    // check whether the payment_status is Completed
    // check that txn_id has not been previously processed
    // check that receiver_email is your Primary PayPal email
    // check that payment_amount/payment_currency are correct
    // process payment
    // assign posted variables to local variables
if(isset($_POST))
{
    $payment_status 	= $_POST['payment_status'];
    $payment_amount		= $_POST['mc_gross'];
    $payment_currency   = $_POST['mc_currency'];
    $txn_id             = $_POST['txn_id'];
    $receiver_email     = $_POST['receiver_email'];
    $payer_email        = $_POST['payer_email'];
	$name		        = $_POST['first_name'];
	$itemcount          = $_POST['num_cart_items'];
	if(isset($_POST['custom']))
	{
		parse_str($_POST['custom'],$_CUSTOMPOST);
		$name=$_CUSTOMPOST['name'];			
		$email= $_CUSTOMPOST['email'];
		$contact=$_CUSTOMPOST['contact'];
		$message= $_CUSTOMPOST['message'];
		$site_url= $_CUSTOMPOST['site_url'];
	}
	$customeremail = $email;
	$subject	="Payment confirmation from ".$site_url;
	$from	=	$receiver_email;
	$message	=	"";
	$currency=$_POST['mc_currency'];
	$items='<table width="80%" border="1" cellspacing="0"><tr><td>Sl No.</td><td>Item name</td><td>Quantity</td><td>Price</td></tr>';
	for($cnt=1;$cnt<=$itemcount;$cnt++)
	{
		if(isset($_POST['item_name'.$cnt]))
		{
		$items.='<tr><td>'.$cnt.'</td><td>'.$_POST['item_name'.$cnt].'</td><td>'.$_POST['quantity'.$cnt].'</td><td>'.$_POST['mc_gross_'.$cnt].'</td></tr>';
		}
	}
	$items.='<tr><td colspan="2" rowspan="4">&nbsp;</td><td>Discount</td><td>'.$_POST['discount'].'</td></tr>';
	$items.='<tr><td><b>Total</b></td><td>'.$_POST['payment_gross']." ".$currency.'</td></tr></table>';
	$message	=	'Dear '.$name.',<br/>

				Thank you for your purchase from '.$site_url.'<br>'.' The details of your purchase are below.<br>

				Transaction ID: '.$txn_id.'<br>'.'
				Transaction Details <br>'.$items.'<br>
				Paid to: '.$receiver_email.'

				<br>Thanks!
				';	
	$message_seller	= 'Hi Admin ,

				This is the confirmation mail from '.$site_url.'<br>'.' The details of sale are below.<br>

				Transaction ID: '.$txn_id.'<br>'.'
				Transaction Details <br>'.$items.'<br>
				Paid to: '.$receiver_email.'

				<br>Thanks!
				';	
		if($payment_status=='Completed'){ 

			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers .= 'From: ' .$from. "\r\n" .
    		'Reply-To: ' .$from . "\r\n";
			//mail to buyer
			 mail( $payer_email, $subject, $message, $headers );
			 mail( $receiver_email, $subject, $message, $headers );
			 mail( $email, $subject, $message, $headers );
		}
		
}