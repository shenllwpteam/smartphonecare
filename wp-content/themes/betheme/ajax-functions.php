<?php
// add the ajax fetch js
add_action( 'wp_footer', 'ajax_fetch' );
function ajax_fetch() {
?>
<script type="text/javascript">
function fetch(){
	var search_key = jQuery('#ajax-search').val();
	if (search_key == '') {
		jQuery('#datafetch').removeClass('show');
	} else {
		jQuery('#datafetch').addClass('show');
	}
	jQuery.ajax({
		url: '<?php echo admin_url('admin-ajax.php'); ?>',
		type: 'post',
		data: { action: 'data_fetch', keyword: search_key },
		success: function(data) {
			jQuery('#datafetch').html( data );
		}
	});
}
</script>

<?php
}

// the ajax function
add_action('wp_ajax_data_fetch' , 'data_fetch');
add_action('wp_ajax_nopriv_data_fetch','data_fetch');
function data_fetch(){

    $the_query = new WP_Query( array( 'posts_per_page' => -1, 's' => esc_attr( $_POST['keyword'] ), 'post_type' => 'page' ) );
    if( $the_query->have_posts() ) {
        while( $the_query->have_posts() ): $the_query->the_post(); ?>

            <h2><a href="<?php echo esc_url( post_permalink() ); ?>"><?php the_title();?></a></h2>

        <?php endwhile;
        wp_reset_postdata();  
    } else { ?>
    	<h2>No matches</h2>
    <?php }
    die();
}

function sp_import_user_details(){
	add_menu_page('Services List', 'Services List', 'manage_options', 'service-import', 'sp_import_callback', 'dashicons-media-spreadsheet'); 
}
add_action('admin_menu', 'sp_import_user_details');
function sp_import_callback(){
	require_once(get_stylesheet_directory().'/service-details.php');
}

//export home owners
if( isset($_POST["download-latest"]) && !empty($_POST["download-latest"]) ) {
	sp_export_services();
}
function sp_export_services(){
	global $wpdb;
	$prefix 	= 	$wpdb->prefix;
	$service_table = 	$prefix.'services';
	if (wp_verify_nonce($_POST['_wpnonce'], 'spnonce_export_services')) {
	    $column_header = array("category","sub_category","service");

		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private", false);
		header("Content-Type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"Service_list.csv\";" );
		header("Content-Transfer-Encoding: binary");

	    $service_list = $wpdb->get_results("SELECT * from {$service_table}", ARRAY_A);
	    if(count($service_list) > 0) {
	        $output = fopen('php://output', 'w');
	        fputcsv($output, $column_header);
	        foreach($service_list as $service) {
	            unset($service['service_id']);
	            fputcsv($output, $service);
	        }
	        fclose($output);
	        exit;
	    }
	}
}
add_action('wp_ajax_sp_sub_category' , 'sp_sub_category');
add_action('wp_ajax_nopriv_sp_sub_category','sp_sub_category');
function sp_sub_category(){
	global $wpdb;
	$category = $_POST['category'];
	$sub_cat = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}services WHERE category like '{$category}'", ARRAY_A );
	$sub_cat_list = array();
	foreach ($sub_cat as $s_cat) {
		$sub_cat_list[] = $s_cat['sub_category'];
	}
	$sub_cat_list = array_unique($sub_cat_list);
	$content = '<option selected="selected">--Vælg--</option>';
	foreach ($sub_cat_list as $sub_cat) {
		$content.= "<option value='{$sub_cat}'>{$sub_cat}</option>";
	}
	die ($content);
}

add_action('wp_ajax_sp_services' , 'sp_services');
add_action('wp_ajax_nopriv_sp_services','sp_services');
function sp_services() {
	global $wpdb;
	$ret_array = array();
	$sub_category = $_POST['sub_category'];
	$page = get_page_by_title($sub_category);
	$slug = $page->post_name;

	$services = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}services WHERE sub_category like '{$sub_category}'", ARRAY_A );
	$services_list = array();
	foreach ($services as $service) {
		$services_list[] = $service['service'];
	}
	$services_list = array_unique($services_list);
	$content = '<option selected="selected">--Vælg--</option>';
	foreach ($services_list as $service) {
		$content.= "<option value='{$service}'>{$service}</option>";
	}
	$ret_array["content"] = $content;
	$ret_array["subcat_slug"] = $slug;

	die (json_encode($ret_array));
}