<?php
/**
 * The template for displaying the footer.
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */


$back_to_top_class = mfn_opts_get('back-top-top');

if( $back_to_top_class == 'hide' ){
	$back_to_top_position = false;
} elseif( strpos( $back_to_top_class, 'sticky' ) !== false ){
	$back_to_top_position = 'body';
} elseif( mfn_opts_get('footer-hide') == 1 ){
	$back_to_top_position = 'footer';
} else {
	$back_to_top_position = 'copyright';
}

?>


<!-- #Footer -->		
<footer id="Footer" class="clearfix">
	
	<?php if ( $footer_call_to_action = mfn_opts_get('footer-call-to-action') ): ?>
	<div class="footer_action">
		<div class="container">
			<div class="column one column_column">
				<?php echo do_shortcode( $footer_call_to_action ); ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
	
	<?php 
		$sidebars_count = 0;
		for( $i = 1; $i <= 5; $i++ ){
			if ( is_active_sidebar( 'footer-area-'. $i ) ) $sidebars_count++;
		}
		
		if( $sidebars_count > 0 ){
			
			$footer_style = '';
				
			if( mfn_opts_get( 'footer-padding' ) ){
				$footer_style .= 'padding:'. mfn_opts_get( 'footer-padding' ) .';';
			}
			
			echo '<div class="widgets_wrapper" style="'. $footer_style .'">';
				echo '<div class="container">';
						
					if( $footer_layout = mfn_opts_get( 'footer-layout' ) ){
						// Theme Options

						$footer_layout 	= explode( ';', $footer_layout );
						$footer_cols 	= $footer_layout[0];
		
						for( $i = 1; $i <= $footer_cols; $i++ ){
							if ( is_active_sidebar( 'footer-area-'. $i ) ){
								echo '<div class="column '. $footer_layout[$i] .'">';
									dynamic_sidebar( 'footer-area-'. $i );
								echo '</div>';
							}
						}						
						
					} else {
						// Default - Equal Width
						
						$sidebar_class = '';
						switch( $sidebars_count ){
							case 2: $sidebar_class = 'one-second'; break;
							case 3: $sidebar_class = 'one-third'; break;
							case 4: $sidebar_class = 'one-fourth'; break;
							case 5: $sidebar_class = 'one-fifth'; break;
							default: $sidebar_class = 'one';
						}
						
						for( $i = 1; $i <= 5; $i++ ){
							if ( is_active_sidebar( 'footer-area-'. $i ) ){
								echo '<div class="column '. $sidebar_class .'">';
									dynamic_sidebar( 'footer-area-'. $i );
								echo '</div>';
							}
						}
						
					}
				
				echo '</div>';
			echo '</div>';
		}
	?>


	<?php if( mfn_opts_get('footer-hide') != 1 ): ?>
	
		<div class="footer_copy">
			<div class="container">
				<div class="column one">

					<?php 
						if( $back_to_top_position == 'copyright' ){
							echo '<a id="back_to_top" class="button button_js" href=""><i class="icon-up-open-big"></i></a>';
						}
					?>
					
					<!-- Copyrights -->
					<div class="copyright">
						<?php 
							if( mfn_opts_get('footer-copy') ){
								echo do_shortcode( mfn_opts_get('footer-copy') );
							} else {
								echo '&copy; '. date( 'Y' ) .' '. get_bloginfo( 'name' ) .'. All Rights Reserved. <a target="_blank" rel="nofollow" href="http://muffingroup.com">Muffin group</a>';
							}
						?>
					</div>

					<?php 
						if( has_nav_menu( 'social-menu-bottom' ) ){
							mfn_wp_social_menu_bottom();
						} else {
							get_template_part( 'includes/include', 'social' );
						}
					?>
							
				</div>
			</div>
		</div>
	
	<?php endif; ?>
	
	<?php 
		if( $back_to_top_position == 'footer' ){
			echo '<a id="back_to_top" class="button button_js in_footer" href=""><i class="icon-up-open-big"></i></a>';
		}
	?>

</footer>

</div><!-- #Wrapper -->

<?php 
	// Responsive | Side Slide
	if( mfn_opts_get( 'responsive-mobile-menu' ) ){
		get_template_part( 'includes/header', 'side-slide' );
	}
?>

<?php
	if( $back_to_top_position == 'body' ){
		echo '<a id="back_to_top" class="button button_js '. $back_to_top_class .'" href=""><i class="icon-up-open-big"></i></a>';
	}
?>

<?php if( mfn_opts_get('popup-contact-form') ): ?>
	<div id="popup_contact">
		<a class="button button_js" href="#"><i class="<?php mfn_opts_show( 'popup-contact-form-icon', 'icon-mail-line' ); ?>"></i></a>
		<div class="popup_contact_wrapper">
			<?php echo do_shortcode( mfn_opts_get('popup-contact-form') ); ?>
			<span class="arrow"></span>
		</div>
	</div>
<?php endif; ?>

<?php do_action( 'mfn_hook_bottom' ); ?>
<?php 
$array_menu = wp_get_nav_menu_items('Smartphonecare');
    $menu = array();
    foreach ($array_menu as $m) {
        if (empty($m->menu_item_parent)) {
            $menu[$m->ID] = array();
            $menu[$m->ID]['id']      =   $m->ID;
            $menu[$m->ID]['active'] = false;                              
            $menu[$m->ID]['name']       =   $m->title;
            $menu[$m->ID]['url']         =   $m->url;
            $menu[$m->ID]['currentPage'] = false;
            $menu[$m->ID]['isShortcut'] = false;         
        }
    }
    $submenu = array();
    foreach ($array_menu as $m) {
        if ($m->menu_item_parent) {
            $submenu[$m->id] = array();
            $submenu[$m->ID]['ID']       =   $m->ID;
            $submenu[$m->ID]['active'] = false;                              
            $submenu[$m->ID]['name']    =   $m->title;
            $submenu[$m->ID]['url']  =   $m->url;
            $submenu[$m->ID]['currentPage'] = false;
            $submenu[$m->ID]['isShortcut'] = false;            
            $menu[$m->menu_item_parent]['children'][] = $submenu[$m->ID];
        }
    }

?>
<script>

        
            window.__INITIAL_STATE__ = {}
            window.__INITIAL_STATE__.menu = {"navigation":<?php echo json_encode( array_values($menu) );?>,"shortcuts":{"bar":[],"list":[]},"sections":{"navigation":{"id":11,"title":"Menu","icon":"Hamburger","order":1}},"business":false};
        

    window.menuConfig = {
        apiHost: '<?php echo site_url();?>'
    };
     var site_url = '<?php echo site_url();?>';


	
    jQuery(document).ready(function() {
	  checkContainer();
	});

	function checkContainer () {
	  if(jQuery('#tredk-menu').html()!=''){
	  	jQuery('#tredk-menu').find('li.sectionItem---QQPZD').after('<ul><li class="link-item mob-menu-items"><a class="href-link" href="http://status.smartphonecare.dk/"><img src="'+site_url+'/wp-content/themes/betheme/images/status.png" class="search-icon"><span>Status</span></a></li><li class="link-item mob-menu-items"><a class="href-link" href="http://webshop.smartphonecare.dk/"><img src="'+site_url+'/wp-content/themes/betheme/images/shop.png" class="search-icon"><span>Shop</span></a></li><li class="link-item mob-menu-items"><a class="href-link" href="http://smartphonecare.dk/kontakt/"><img src="'+site_url+'/wp-content/themes/betheme/images/call.png" class="search-icon"><span>Kontakt</span></a></li></ul>');
	  	jQuery('#tredk-menu').find('section.sections---3gZVz').after('<section class="sections---3gZVz search-section"><ul class="none---1EJzg"><li class="sectionItem---QQPZD"><a class="sectionLink---1e5Tr link---22ejX desktop-href-link" href="http://status.smartphonecare.dk/"><img src="'+site_url+'/wp-content/themes/betheme/images/status.png" class="search-icon"><span>Status</span></a></li></ul></section>');
	  	jQuery('#tredk-menu').find('.sections---3gZVz.search-section li.sectionItem---QQPZD').after('<section class="sections---3gZVz"><ul class="none---1EJzg"><li class="sectionItem---QQPZD"><a class="sectionLink---1e5Tr link---22ejX desktop-href-link" href="http://webshop.smartphonecare.dk/"><img src="'+site_url+'/wp-content/themes/betheme/images/shop.png" class="search-icon"><span>Shop</span></a></li></ul></section>');
	  } else {
	    setTimeout(checkContainer, 50); //wait 50 ms, then try again
	  }
	}
</script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/smartphone-menu.js"></script>
<script type="text/javascript" data="media-js" src="<?php echo get_template_directory_uri(); ?>/js/matchMedia.js"></script>
<script>
	jQuery('.sectionLink---1e5Tr').on('click', function () {
		var current_width = jQuery(window).width();
		if (current_width < 768) {
			jQuery('body').addClass('show');
			if (jQuery(".foldout---2mz34").length > 0) {
				jQuery('body').removeClass('show');
				jQuery('.foldout---2mz34').css('width', '-280px');
			}
		} else {
			jQuery('body').toggleClass('show');
			jQuery('.foldout---2mz34').css('width', '-280px');
		}
		setTimeout(function(){
			jQuery('.foldout---2mz34 div.content---3CudV').append('<input type="text" placeholder="Søg" name="keyword" id="ajax-search" onkeyup="fetch()"><i id="search-icon" class="fa fa-search" aria-hidden="true"></i></input><div id="datafetch"></div>');
		}, 300);
	});
	jQuery('#search-category').on('change', function () {
		var search_key = jQuery('#search-category').val();
		jQuery('#sub-category').html( '<option selected="selected">--Vælg--</option>' );
		jQuery('#services').html( '<option selected="selected">--Vælg--</option>' );
		jQuery.ajax({
			url: '<?php echo admin_url('admin-ajax.php'); ?>',
			type: 'post',
			data: { action: 'sp_sub_category', category : search_key },
			success: function(data) {
				jQuery('#sub-category').html( data );
			}
		});
	});
	jQuery('#sub-category').on('change', function () {
		var search_key = jQuery('#sub-category').val();
		jQuery('#services').html( '<option selected="selected">--Vælg--</option>' );
		jQuery.ajax({
			url: '<?php echo admin_url('admin-ajax.php'); ?>',
			type: 'post',
			data: { action: 'sp_services', sub_category : search_key },
			success: function(data) {
				var response_data = JSON.parse(data);
				jQuery('#services').html( response_data.content );
				if (search_key == '--Vælg--') {
					jQuery('#service_url').attr( 'href', '' );
				} else {
					jQuery('#service_url').attr( 'href', response_data.subcat_slug + '/' );
				}
			}
		});
	});
	jQuery('#services').on('change', function () {
		var search_key = jQuery('#services').val();
		var current_url = jQuery('#service_url').attr( 'href' );
		current_url = current_url.split('?')[0];
		if (search_key == '--Vælg--') {
			jQuery('#service_url').attr( 'href', current_url );
		} else {
			jQuery('#service_url').attr( 'href', current_url + '?service=' + search_key );
		}
	});
	
	// Read a page's GET URL variables and return them as an associative array.
	function getUrlVars() {
		var vars = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		for(var i = 0; i < hashes.length; i++)
		{
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		var service = decodeURIComponent(vars['service']);
		jQuery('tr:contains("' + service + '")').find("td").addClass('highlight-service');
	}
	getUrlVars();

</script>
<!-- wp_footer() -->
<?php wp_footer(); ?>

<style type="text/css">
	.canvas---A_OfV, .foldout---2mz34, .foldoutContainer---3qRz6 {
		max-width: 0px;
	}
	.show .canvas---A_OfV, .show .foldout---2mz34, .show .foldoutContainer---3qRz6 {
		max-width: 100%;
	}
	.foldout---2mz34 {
		display: none;
	}
	.show .foldout---2mz34 {
		display: block;
	}
	html {
		overflow-y: visible !important;
	}
	.contact-link{
		text-align:center;
		display: block;
	}
	.contact-item{
		background:#0C70AB;
	}
	.link-item{
		display: none;
	}
	#Top_bar .top_bar_right_wrapper{
		top:11px;
	}
	#Top_bar .top_bar_right, .header-plain #Top_bar .top_bar_right{
		height: 65px;
	}
	#Top_bar .menu > li > a{
		padding: 3px 0;
	}
	#Top_bar #logo, .header-fixed #Top_bar #logo, .header-plain #Top_bar #logo, .header-transparent #Top_bar #logo{
		padding: 3px 0;
	}
	#Top_bar.is-sticky{
		top:-6px !important;
	}
	.sectionItem---QQPZD, .link-item{
		margin-top: 5px;
	}
	#ajax-search {
		opacity: 0;
		visibility: hidden;
		left: auto;
		top: 0;
		position: absolute;
		width: 0px;
		right: 0px;
		overflow: hidden;
		-webkit-transition: all 0.34s ease 0s;
		transition: all 0.34s ease 0s;
		border: transparent solid 1px;
	}
	body {
		right: 0;
		-webkit-transition: all 0.34s ease 0s;
		transition: all 0.34s ease 0s;
	}
	body.show {
		right: 220px;
	}
	body.show:after {
		content: '';
		height: 100%;
		background-color: rgba(0,0,0,.5);
		width: 100%;
		display: block;
		position: absolute;
		top: 0;
		z-index: 999;
	}
	.show #ajax-search {
		visibility: visible;
		width: 360px;
		opacity: 1;
		border: #0278B8 solid 1px;
	}
	#search-icon {
		opacity: 0;
		visibility: hidden; 
		left: auto;
		top: 0;
		color: #444;
		position: absolute;
		font-size: 20px;
		padding: 7px;
		right: 0px;
		overflow: hidden;
		-webkit-transition: all 0.3s ease 0s;
		transition: all 0.3s ease 0s;
	}
	.show #search-icon {
		visibility: visible;
		opacity: 1;
	}
	#datafetch {
		opacity: 0;
		visibility: hidden;
		left: auto;
		background-color: #0C70AB;
		top: 37px;
		padding: 15px;
		position: absolute;
		width: 360px;
		right: 0px;
		overflow: hidden;
		-webkit-transition: all 0.3s ease 0s;
		transition: all 0.3s ease 0s;
	}
	#datafetch h2 {
		color: #fff;
		font-size: 17px;
		line-height: 18px;
		font-family: HelveticaNeueLT-Bold;
		margin-bottom: 7px;
	}
	#datafetch h2 a {
		color: #fff;
		font-family: HelveticaNeueLT-Bold;
	}
	#datafetch.show {
		opacity: 1;
		visibility: visible;
	}
	.mfn-main-slider {
		padding-top: 70px;
		background-color: #fff;
	}
	#Content {
		padding-top: 100px;
	}
	#tredk-menu {
		position: fixed;
		z-index: 99999;
	}
	@media only screen and (min-width: 768px){
		.search-section{
			display: block;
		}
		.activeSectionLink---2Z3s4{
			/*border-bottom:3px solid #218ECE !important;*/
			border-left-color: transparent;
			border-right-color: transparent;
		}
		.sectionLink---1e5Tr{
			color:#fff;
			opacity: 1;
		}
		.sectionLink---1e5Tr span{
			margin-bottom: 13px;
    		margin-top: 5px;
    		font-family: "Roboto", Arial, Tahoma, sans-serif;
		}
		.sectionItem---QQPZD{
			margin-left: 15px;
			margin-right: 15px;
		}
		.sectionLink---1e5Tr img, .href-link img {
		    margin-bottom: 0px;
    		margin-top: 0px;
			min-height: auto;
    		transform: none !important;
		}
		.sectionItem---QQPZD:first-child{
			margin-top: 15px !important
		}
		.search-section{
			margin-top:10px;
		}
	}
	@media only screen and (max-width: 767px){
		body.show {
			right: 260px;
		}
		.link-item{
			display: block;
		}
		.search-section{
			display: none;
		}
		.sectionLink---1e5Tr {
			float: right;
			width: 130px;
			margin-top: -8px;
		}
		.sectionLink---1e5Tr, .href-link{
		    padding: 24px 0 0px;
		    color:#fff;
		    opacity: 1;
		}
		.sectionLink---1e5Tr:active, .sectionLink---1e5Tr:focus, .sectionLink---1e5Tr:hover{
			text-decoration: none !important;
		}
		.logo---F1oJ0 a{
			height: calc(100%) !important;
		}
		.activeSectionLink---2Z3s4, .activeSectionLink---2Z3s4:active, .activeSectionLink---2Z3s4:focus{
			border-bottom-color:transparent;
			text-decoration: none !important;
		}
		ul.none---1EJzg {
			max-height: 90px;
			min-height: 90px;
			background-color: #087CBE;
		}
		ul.none---1EJzg li {
			margin-top: -10px;
			min-width: 90px;
			max-height: 100px;
			border-left: none;
		}
		ul.none---1EJzg li.animationBase---shuCD {
			background-color: #0C70AB;
			padding-bottom: 7px;
		}
		ul.none---1EJzg a {
			text-decoration: none;
			height: auto;
		}
		ul.none---1EJzg span{
			padding-bottom:3px;
			font-size: 18px;
		}
		img.contact-icon{
			max-width: 60px;
		}
		.contact-item{
			height: auto;
			margin: 5px 80px;
		}
		.contact-link{
			margin-top: 11px;
		}
		.sectionItem---QQPZD, .mob-menu-items{
			margin-top: 0px;
			padding: 0;
		}
		.sectionLink---1e5Tr img, .href-link img {
		    width: 1em;
		    height: 1em;
		    margin-bottom: 5px;
			transform: none !important;
		}
		.link-item{
			opacity: 1;
			--webkit-transform: translateY(0);
			transform: translateY(0);
		}
		.mobile-header-mini.mobile-mini-ml-ll #Top_bar .logo{
		 margin-left:0 !important;
		}
		.mobile-header-mini #Top_bar .logo{
		 float:none !important;
		}
		.responsive-menu-toggle {
			display: none !important;
		}
		#Footer {
	 		padding-bottom:25vw;
		}
		#ajax-search {
			top: 90px;
 			right: 0;
		}
		#search-icon {
			top: 90px;
			right: 0;
		}
		#datafetch {
			top: 120px;
			width: 280px;
			right: 0;
		}
		.siteNavigation---OhFkA ul.none---1EJzg {
			max-height: 100%;
			background-color: transparent;
		}
		.siteNavigation---OhFkA ul.none---1EJzg li {
			min-height: initial;
			max-height: 100%;
			padding: 0;
			margin-top: 0;
		}
		.siteNavigation---OhFkA ul.none---1EJzg a {
			font-size: 19px;
			opacity: 1;
			font-family: PT Sans,sans-serif;
			font-weight: 500;
		}
		.siteNavigation---OhFkA ul .item---1Xn0z>span:first-of-type {
			padding: 10px 0 10px 15px;
			display: block;
		}
		ul.none---1EJzg span.container---3DjcC {
			padding-bottom: 5px;
		}
		ul.none---1EJzg .subList---3fh8h {
			margin-left: 0;
			background-color: #0C70AB;
			padding-top: 0px;
			margin-top: 0px;
			margin-bottom: 0;
		}
		.siteNavigation---OhFkA ul ul.subList---3fh8h a {
			font-size: 18px;
			font-family: PT Sans,sans-serif;
		}
		.siteNavigation---OhFkA ul .subList---3fh8h .item---1Xn0z>span:first-of-type {
			border: none;
		}
		.siteNavigation---OhFkA ul.none---1EJzg .subList---3fh8h li {
			border: none;
		}
		ul.none---1EJzg span.line---drHZX {
			display: none;
		}
		.show #ajax-search {
			width: 280px;
		}
		.right---3Ttn2 {
			padding-top: 10px;
			padding-left: 10px;
		}
	}
	@media only screen and (max-width: 499px){
		ul.none---1EJzg li {
			min-width: 80px;
		}
		.content---3CudV {
			padding: 100px 0px 0;
		}
	}
	.service-list {
		background-repeat: no-repeat;
		background-position: -75px top;
		overflow: hidden;
		padding: 20px 0px 0px 0px;
	}
	.service-list select, .service-list a {
		margin-right: 35px;
	}
	.services-shortcode {
		margin: -35px auto 30px;
		padding-left: 75px;
	}
	.service-list a {
		background-color: #0095eb;
		color: #fff;
		text-decoration: none;
		padding: 6px 19px;
		width: auto;
		max-height: 24px;
	}
	.service-list .serach-form {
		clear: both;
		overflow: hidden;
		display: flex;
	}
	.service-list .search-category:before {
		content: "1.";
	}
	.service-list .sub-category:before {
		content: "2.";
	}
	.service-list .services:before {
		content: "3.";
	}
	.service-list .search-drop-down:before {
		color: #FFF;
		font-size: 14px;
		float: left;
		padding: 5px;
		display: block;
		background: #343083;
		background: linear-gradient(135deg,#2c88d8 0,#3c9fd8 100%);
	}
	#Top_bar .menu > li.current-menu-item > a, #Top_bar .menu > li.current_page_item > a, #Top_bar .menu > li.current-menu-parent > a, #Top_bar .menu > li.current-page-parent > a, #Top_bar .menu > li.current-menu-ancestor > a, #Top_bar .menu > li.current-page-ancestor > a, #Top_bar .menu > li.current_page_ancestor > a, #Top_bar .menu > li.hover > a {
		color: #2991d6;
	}
	@media(max-width: 767px) {
		.foldout---2mz34 {
			animation-name: example;
			animation-duration: 0.50s;
		}

		@-webkit-keyframes example {
			0% {background-color:#038CD6; right:-150px; top:0px;}
			100% {background-color:#038CD6; right:0px; top:0px;}
		}
	}
	@media only screen and (max-width: 1199px){
		.service-list select, .service-list a {
			margin-right: 10px;
			max-width: 200px;
		}
	}
	@media only screen and (max-width: 991px){
		.services-shortcode { 
			padding: 0px;
		}
		.service-list .serach-form {
			display: block;
		}
		.service-list select {
			width: 90%;
			max-width: 90%;
		}
	}
	@media only screen and (max-width: 767px){
		.services-shortcode {
			padding-left: 0px!important;
			padding-right: 0px!important;
		}
		.service-list select {
			max-width: 85%;
		}
	}
</style>
</body>
</html>
	