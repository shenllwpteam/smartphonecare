<?php
global $wpdb;
$prefix = $wpdb->prefix;
$service_table = $prefix.'services';

if (!empty($_FILES['csv_upload']['name'])) {
    $nonce=$_POST['_wpnonce'];

    if (wp_verify_nonce($nonce, 'spnonce_import_service')) {
        
        $upload_dir = wp_upload_dir();
        if(!is_dir($upload_dir["basedir"]  .'/sp-serivce-list')) {
            mkdir($upload_dir["basedir"]  .'/sp-serivce-list');
        }
        $upload_path = $upload_dir["basedir"]  .'/sp-serivce-list/' . $_FILES['csv_upload']['name'];
        move_uploaded_file($_FILES['csv_upload']['tmp_name'], $upload_path);
        $csv_file = fopen($upload_path,"r");
        while(($result = fgetcsv($csv_file)) !== false) {
            $result = array_map("utf8_encode", $result);
            $csv_data[] =  $result;
        }
        fclose($csv_file);
        $csv_columns = $csv_data[0];
        for($i=0; $i < count($csv_columns); $i++) {
            $csv_columns[$i] = strtolower($csv_columns[$i]);
        }
        $service_list_columns = array('category', 'sub_category', 'service');
        if(sizeof($service_list_columns) == sizeof($csv_columns)) {
            $csv_data = array_slice($csv_data, 1);
            foreach($csv_data as $single_row) {
                $get_row = $wpdb->get_results("SELECT * from {$user_table}", ARRAY_A);
                $row_count =    count($get_row);
                $data_array=    array( 
                                    'category' => isset($single_row[0]) ? $single_row[0] : '',
                                    'sub_category' => isset($single_row[1]) ? $single_row[1] : '',
                                    'service' => isset($single_row[2]) ? $single_row[2] : '',
                                );
                $insert_status = $wpdb->insert( $service_table, $data_array );
            }  
            exit();
            wp_safe_redirect(admin_url().'admin.php?page=service-import&msg=valid');
            exit; 
        } else {
            // wp_safe_redirect(admin_url().'admin.php?page=service-import&msg=invalid');
            exit;
        }
    }
}

if (isset($_GET['msg'])) {
    if ($_GET['msg'] === 'invalid') : ?>
        <div class="notice notice-error is-dismissible">
            <p><?php _e('Column mismatch'); ?></p>
        </div>
    <?php else : ?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e('Imported successfully'); ?></p>
        </div>
    <?php endif;
}
?>
<h3>Service Details</h3>
<form  method="post" enctype="multipart/form-data">
    <?php wp_nonce_field('spnonce_import_service');?>
    <label style="padding-right: 15px;">Service list</label><input type='file' id='csv_upload' name='csv_upload'></input>
    <p class="submit">
        <input type="submit" name="submit" id="submit" class="button button-primary" value="Import">
        <a class='button button-primary' href='<?php echo get_stylesheet_directory_uri()."/service_list.csv";?>'>Download sample</a>
    </p>
</form>

<h3 class='user-list'>Services List</h3>
<form method="post" class='user-export-form'>
    <?php wp_nonce_field('spnonce_export_services');?>
    <input type="submit" name="download-latest" id="download-latest" class="button button-primary" value="Download Latest">
</form>
<table class='wp-list-table widefat fixed striped'>
    <tr>
        <th>S.No</th>
        <th>Brand</th>
        <th>Modal</th>
        <th>Service</th>
    </tr>

<?php
    $service_list = $wpdb->get_results("SELECT * from {$service_table}", ARRAY_A);
    if(count($service_list) > 0) {
        foreach($service_list as $row) {
            $i = $i + 1;
?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row["category"];?></td>
            <td><?php echo $row["sub_category"];?></td>
            <td><?php echo $row["service"];?></td>
        </tr>
<?php
        }
    } else {
        echo '<td colspan="10">No Records Found</td>';
    }
?>
</table>
