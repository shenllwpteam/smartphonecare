<?php
/**
 * WOO-RFQ-List
 *
 * @author  Neah Plugins
 * @package RFQ-ToolKit
 */

error_reporting(0);

?>
<noscript>
    <H1> Javascript is required for this page. Please enable JavaScript to continue.</h1>
</noscript>

<?php



gpls_woo_rfq_print_notices();


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
require_once(ABSPATH . 'wp-settings.php');

//$gpls_woo_rfq_cart = get_transient(gpls_woo_rfq_cart_tran_key() . '_' . 'gpls_woo_rfq_cart');
$gpls_woo_rfq_cart = gpls_woo_rfq_get_item(gpls_woo_rfq_cart_tran_key() . '_' . 'gpls_woo_rfq_cart');

//d($gpls_woo_rfq_cart);

if (($gpls_woo_rfq_cart == false)) {
    wc_get_template('woo-rfq/rfq-cart-empty.php', array());

    exit;
}

if (isset($_REQUEST['order_id']) && ($_REQUEST['order_id'] != false)) {

    $order_factory = new WC_Order_Factory();
    $order = $order_factory->get_order($_REQUEST['order_id']);
    wc_get_template( 'checkout/thankyou.php', array( 'order' => $order ) );

    exit;
}


?>
<div id="rfq_cart_wrapper" class="rfq_cart_wrapper">
<div class="woo_rfq_top_html_desc" >

    <?php do_action('gpls_woo_rfq_request_page_top_html_desc') ; ?>

</div>
<?php
$wc_get_update_url = pls_woo_rfq_get_link_to_rfq();

?>
<div style="clear: both"></div>

<?php
$gpls_woo_rfq_styles =array();

$gpls_woo_rfq_page_style ='';

$gpls_woo_rfq_styles = apply_filters('gpls_woo_rfq_before_cart_gpls_woo_rfq_styles',$gpls_woo_rfq_styles);
$gpls_woo_rfq_page_style = apply_filters('gpls_woo_rfq_page_style',$gpls_woo_rfq_styles);




?>

<?php do_action('gpls_woo_rfq_before_cart'); ?>
    <form name="rfqform" id="rfqform" class="rfqform" action="<?php echo $wc_get_update_url; ?>" method="post" enctype="multipart/form-data">
<?php $nonce = wp_create_nonce('gpls_woo_rfq_handle_rfq_cart_nonce') ; ?>
<div class="woocommerce gpls_woo_rfq_request_page">

    <div class="woocommerce gpls_woo_rfq_request_cart">
    <div style="clear: both"></div>
        <table id="rfq_cart_shop_table" class="shop_table shop_table_responsive cart rfq_cart_shop_table" cellspacing="0" >

            <tr class="cart_tr">
                <th class="product-remove cart_th">&nbsp;</th>
                <th class="product-thumbnail cart_th">&nbsp;</th>
                <th class="product-name cart_th"><?php printf( __('Product', 'woo-rfq-for-woocommerce')); ?></th>
                <th class="product-quantity cart_th"><?php printf( __('Quantity', 'woo-rfq-for-woocommerce')); ?></th>

            </tr>

            <?php do_action('gpls_woo_rfq_before_cart_contents'); ?>

            <?php



            foreach ($gpls_woo_rfq_cart as $cart_item_key => $cart_item) {




                $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);




                //d($cart_item);
                $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

                // $_product   = $cart_item['data'];
                //$product_id = $cart_item['data']['id'];


                if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('gpls_woo_woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                    $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
                    ?>
                    <tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

                        <td class="product-remove cart_td ">
                            <?php

                            if (isset($cart_item['bundled_by']) && isset($cart_item['bundled_by'])) {
                                echo '';
                            } else {
                                $url = esc_url($wc_get_update_url) . "?remove_rfq_item=" . $cart_item_key;
                                echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
                                    '<a href="%s" type="submit" class="remove gpls_product_remove" title="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
                                    $url . '&man-deleted=' . $cart_item_key."&gpls_woo_rfq_nonce=".$nonce,
                                    __('Remove this item', 'woo-rfq-for-woocommerce'),
                                    esc_attr($product_id),
                                    esc_attr($_product->get_sku())
                                ), $cart_item_key);


                            }


                            ?>

                        </td>

                        <td class="product-thumbnail cart_td">
                            <?php
                            $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

                            if ( ! $product_permalink ) {
                                echo $thumbnail;
                            } else {
                                printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
                            }
                            ?>
                        </td>

                        <td class="product-name  cart_td" data-title="<?php printf( __('Product', 'woo-rfq-for-woocommerce')); ?>">
                            <?php
                            if (!$product_permalink) {
                                echo apply_filters('woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key) . '&nbsp;';
                            } else {
                                echo apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s</a>', esc_url($product_permalink), $_product->get_title()), $cart_item, $cart_item_key);
                            }

                            // Meta data
                            rfq_cart_get_item_data($cart_item);

                            do_action('gplsrfq_cart_item_product',$_product, $cart_item, $cart_item_key);

                            do_action('gpls_woo_rfq_get_product_extra',$_product,$cart_item, $cart_item_key);



                            // Backorder notification
                            if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                                echo '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woo-rfq-for-woocommerce') . '</p>';
                            }
                            ?>
                        </td>



                        <td class="product-quantity  cart_td" data-title="<?php printf( __('Quantity', 'woo-rfq-for-woocommerce')); ?>">
                            <?php


                            if ($_product->is_sold_individually()) {
                                $product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
                            } else if ((isset($cart_item['bundled_by']) && isset($cart_item['bundled_by']))
                                || (isset($cart_item['bundled_items']) && isset($cart_item['bundled_items']) )
                            ) {
                                $product_quantity = sprintf("{$cart_item['quantity']} <input type='hidden' name='cart[%s][qty]' value='{$cart_item['quantity']}' />", $cart_item_key);
                            }else if ((isset($cart_item['composite_parent']) && isset($cart_item['composite_parent']))
                                || (isset($cart_item['composite_children']) && isset($cart_item['composite_children']) )
                            ) {
                                $product_quantity = sprintf("{$cart_item['quantity']} <input type='hidden' name='cart[%s][qty]' value='{$cart_item['quantity']}' />", $cart_item_key);
                            }

                            else {
                                $product_quantity = woocommerce_quantity_input(array(
                                    'input_name' => "cart[{$cart_item_key}][qty]",
                                    'input_value' => $cart_item['quantity'],
                                    'max_value' => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                                    'min_value' => '0',
                                    ''
                                ), $_product, false);
                            }
                    if ((isset($cart_item['bundled_items']) && isset($cart_item['bundled_items']) )) {
                        echo '<b style="padding-left: 1.3em">'.$product_quantity.'</b>';
                    }else if ((isset($cart_item['composite_children']) && isset($cart_item['composite_children']) )) {
                                echo '<b style="padding-left: 1.3em">'.$product_quantity.'</b>';
                    }
                    else{
                        echo $product_quantity;
                    }


                            ?>
                        </td>


                    </tr>
                    <?php
                }
            }
            ?>

            <?php

            do_action('gpls_woo_rfq_after_cart_contents');

            ?>

            <!-- <tr class="cart_tr">

                <td colspan="6" class="actions cart_td">

                    <?php

                   // $confirmation_message = get_option('rfq_cart_wordings_gpls_woo_rfq_update_rfq_cart_button',__('Update Quote Request', 'woo-rfq-for-woocommerce'));
                    //$confirmation_message = __($confirmation_message,'woo-rfq-for-woocommerce');

                    ?>

                    <div class="update_rfq_cart">
                    
                        <?php do_action('gpls_woo_rfq_cart_actions'); ?>
                    
                        <input   type="submit" class="update-rfq-cart button alt gpls-woo-rfq_update-rfq-cart_button"
                                 id="update_rfq_cart" formnovalidate="formnovalidate"
                                 name="update_rfq_cart" value="<?php echo __($confirmation_message,'woo-rfq-for-woocommerce'); ?>"
                                 onmouseover="<?php echo $gpls_woo_rfq_styles['gpls_woo_rfq_page_update_button_onmouseover'].';'.$gpls_woo_rfq_styles['gpls_woo_rfq_page_update_button_background_onmouseover']; ?>"
                                 onmouseout="<?php echo $gpls_woo_rfq_styles['gpls_woo_rfq_page_update_button_onmouseout'].';'.$gpls_woo_rfq_styles['gpls_woo_rfq_page_update_button_background_onmouseout']; ?>"
                                 style = "margin-right: 1em;<?php echo $gpls_woo_rfq_styles['gpls_woo_rfq_page_update_button_styles'] ?>"
                        />
                    </div>
                </td>
            </tr> -->





        </table>
    </div>
        <?php

        do_action('gpls_woo_rfq_after_items_list');

        ?>

        <div style="clear:both"></div>
        <div class="rfq_shop_table_customer_info_div">
        <table id="rfq-shop-table_customer_info" class="shop_table cart shop_table_responsive rfq-shop-table_customer_info" cellspacing="1"   cellpadding="1" >

            <?php

            do_action('gpls_woo_rfq_before_customer_info_label');

            ?>

            <tr class="info_tr">
                <?php
                $customer_info_label = get_option('settings_gpls_woo_rfq_customer_info_label','Checkout');
                $customer_info_label = __($customer_info_label,'woo-rfq-for-woocommerce');

                if(!isset($customer_info_label)){
                    $customer_info_label = __('Checkout','woo-rfq-for-woocommerce');
                }

                ?>
                <td align="center" colspan="4" class="info_td"  style="text-align: center;">
                    <h1 class="woo-rfq-customer-info-header"><?php echo $customer_info_label; ?></h1>
                    <p class="woo-rfq-customer-info-hint">Once you have completed this information, you get a price immediately</p>
                </td>


            </tr>
        </table>
            <?php do_action('gpls_woo_rfq_cart_actions_customer_bid'); ?>

            <?php if (function_exists('wp_get_current_user')): ?>
            <?php if (!wp_get_current_user()->exists()): ?>
                <div class="rfq-custom-form">
                    <div class="medium-width custom-rfq-field">
                        <label><?php printf( __('Fornavn', 'woo-rfq-for-woocommerce')); ?>
                            <span class="required">*</span>
                        </label>
                        <input type="text" required="required" id="rfq_fname " name="rfq_fname" placeholder=""/>
                    </div>
                    <div class="medium-width custom-rfq-field">
                        <label><?php printf( __('Efternavn', 'woo-rfq-for-woocommerce')); ?>
                            <span class="required">*</span>
                        </label>
                        <input type="text" required="required" id="rfq_lname " name="rfq_lname" placeholder=""/>
                    </div>
                    <div class="medium-width custom-rfq-field">
                        <label><?php printf( __('E-mailadresse', 'woo-rfq-for-woocommerce')); ?>
                            <span class="required">*</span>
                        </label>
                        <input type="text" required="required" id="rfq_email_customer" name="rfq_email_customer" placeholder=""/>
                    </div>
                    <div class="medium-width custom-rfq-field">
                        <label><?php printf( __('Telefon', 'woo-rfq-for-woocommerce')); ?>
                            <span class="required">*</span>
                        </label>
                        <input type="text" required="required" id="rfq_phone" name="rfq_phone" placeholder=""/>
                    </div>
                    <div class="ful-width custom-rfq-field">
                        <label><?php printf( __('Gade/vej', 'woo-rfq-for-woocommerce')); ?>
                            <span class="required">*</span>
                        </label>
                        <input type="text" required="required" id="rfq_address" name="rfq_address" placeholder=""/>
                    </div>
                    <div class="medium-width custom-rfq-field">
                        <label><?php printf( __('Postnummer', 'woo-rfq-for-woocommerce')); ?>
                            <span class="required">*</span>
                        </label>
                        <input type="text" required="required" id="rfq_zip" name="rfq_zip" placeholder=""/>
                    </div>
                    <div class="medium-width custom-rfq-field">
                        <label><?php printf( __('By', 'woo-rfq-for-woocommerce')); ?>
                            <span class="required">*</span>
                        </label>
                        <input type="text" required="required" id="rfq_by" name="rfq_by" placeholder=""/>
                    </div>
                    <div class="medium-width custom-rfq-field">
                        <label><?php printf( __('Reg. nr.', 'woo-rfq-for-woocommerce')); ?>
                            <span class="required">*</span>
                        </label>
                        <input type="text" required="required" id="rfq_reg_nr" name="rfq_reg_nr" placeholder=""/>
                    </div>
                    <div class="medium-width custom-rfq-field">
                        <label><?php printf( __('Kontonummer', 'woo-rfq-for-woocommerce')); ?>
                            <span class="required">*</span>
                        </label>
                        <input type="text" required="required" id="rfq_kontonummer" name="rfq_kontonummer" placeholder=""/>
                    </div>
                    <div class="medium-width custom-rfq-field">
                        <label><?php printf( __('Kommenter', 'woo-rfq-for-woocommerce')); ?>
                            <span class="required">*</span>
                        </label>
                        <textarea rows="10" type="text" required="required" id="rfq_kommenter" name="rfq_kommenter"></textarea>
                    </div>
                    <div class="ful-width rfq-confirm">
                        <label>
                            <input type="checkbox" id="rfq_confirm" name="rfq_confirm">Jeg har læst betingelserne
                        </label>
                    </div>
                    <div class="ful-width submit">
                        <button type="submit">Sælg din enhed</button>
                    </div>
                </div>
            <?php endif; ?>
            <?php endif; ?>
        </div>

        <?php do_action('gpls_woo_woocommerce_after_cart_table'); ?>


</div>
    </form>
<div style="clear: both"></div>

<div class="woo_rfq_bottom_html_desc" >
    <?php
    do_action('gpls_woo_rfq_request_page_bottom_html_desc');

   // gpls_woo_rfq_cart_delete(gpls_woo_rfq_cart_tran_key() . '_' . 'gpls_woo_rfq_cart');



    ?>
</div>

</div>