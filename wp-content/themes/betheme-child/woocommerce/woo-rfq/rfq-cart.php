<?php
/**
 * WOO-RFQ-List
 *
 * @author  Neah Plugins
 * @package RFQ-ToolKit
 */

error_reporting(0);

?>
<noscript>
    <H1> Javascript is required for this page. Please enable JavaScript to continue.</h1>
</noscript>

<?php



gpls_woo_rfq_print_notices();


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
require_once(ABSPATH . 'wp-settings.php');

//$gpls_woo_rfq_cart = get_transient(gpls_woo_rfq_cart_tran_key() . '_' . 'gpls_woo_rfq_cart');
$gpls_woo_rfq_cart = gpls_woo_rfq_get_item(gpls_woo_rfq_cart_tran_key() . '_' . 'gpls_woo_rfq_cart');

//d($gpls_woo_rfq_cart);

if (($gpls_woo_rfq_cart == false)) {
    wc_get_template('woo-rfq/rfq-cart-empty.php', array());

    exit;
}

if (isset($_REQUEST['order_id']) && ($_REQUEST['order_id'] != false)) {

    $order_factory = new WC_Order_Factory();
    $order = $order_factory->get_order($_REQUEST['order_id']);
    wc_get_template( 'checkout/thankyou.php', array( 'order' => $order ) );

    exit;
}


?>
<div id="rfq_cart_wrapper" class="rfq_cart_wrapper">
<div class="woo_rfq_top_html_desc" >

    <?php do_action('gpls_woo_rfq_request_page_top_html_desc') ; ?>

</div>
<?php
$wc_get_update_url = pls_woo_rfq_get_link_to_rfq();

?>
<div style="clear: both"></div>

<?php
$gpls_woo_rfq_styles =array();

$gpls_woo_rfq_page_style ='';

$gpls_woo_rfq_styles = apply_filters('gpls_woo_rfq_before_cart_gpls_woo_rfq_styles',$gpls_woo_rfq_styles);
$gpls_woo_rfq_page_style = apply_filters('gpls_woo_rfq_page_style',$gpls_woo_rfq_styles);




?>

<?php do_action('gpls_woo_rfq_before_cart'); ?>
    <form name="rfqform" id="rfqform" class="rfqform" action="<?php echo $wc_get_update_url; ?>" method="post" enctype="multipart/form-data">
<?php $nonce = wp_create_nonce('gpls_woo_rfq_handle_rfq_cart_nonce') ; ?>
<div class="woocommerce gpls_woo_rfq_request_page">

        <?php

        do_action('gpls_woo_rfq_after_items_list');

        ?>
            <div class="woocommerce gpls_woo_rfq_request_cart">
        <div style="clear: both"></div>
        
    </div>
        <div style="clear:both"></div>
        <div class="rfq_shop_table_customer_info_div animate fadeInUp " data-anim-type="fadeInUp">
        <?php
        $customer_info_label = get_option('settings_gpls_woo_rfq_customer_info_label','Checkout');
        $customer_info_label = __($customer_info_label,'woo-rfq-for-woocommerce');

        if(!isset($customer_info_label)){
            $customer_info_label = __('Checkout','woo-rfq-for-woocommerce');
        }

        ?>
        <h1 class="woo-rfq-customer-info-header"><?php echo $customer_info_label; ?></h1>
        <p class="woo-rfq-customer-info-hint">Når du har gennemført disse oplysninger, får du en pris med det samme</p>
        <table id="rfq_cart_shop_table" class="shop_table shop_table_responsive cart rfq_cart_shop_table animate fadeInUp " cellspacing="0"  data-anim-type="fadeInUp" style="border-collapse: collapse;">

            <tr class="cart_tr">
                <th class="product-remove cart_th">&nbsp;</th>
                <th class="product-thumbnail cart_th">&nbsp;</th>
                <th class="product-name cart_th"><?php printf( __('Product', 'woo-rfq-for-woocommerce')); ?></th>
                <th class="product-quantity cart_th"><?php printf( __('Quantity', 'woo-rfq-for-woocommerce')); ?></th>

            </tr>

            <?php do_action('gpls_woo_rfq_before_cart_contents'); ?>

            <?php

            foreach ($gpls_woo_rfq_cart as $cart_item_key => $cart_item) {


                $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);


                //d($cart_item);
                $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

                // $_product   = $cart_item['data'];
                //$product_id = $cart_item['data']['id'];


                if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('gpls_woo_woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                    $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
                    ?>
                    <tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

                        <td class="product-remove cart_td ">
                            <?php

                            if (isset($cart_item['bundled_by']) && isset($cart_item['bundled_by'])) {
                                echo '';
                            } else {
                                $url = esc_url($wc_get_update_url) . "?remove_rfq_item=" . $cart_item_key;
                                echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
                                    '<a href="%s" type="submit" class="remove gpls_product_remove" title="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
                                    $url . '&man-deleted=' . $cart_item_key."&gpls_woo_rfq_nonce=".$nonce,
                                    __('Remove this item', 'woo-rfq-for-woocommerce'),
                                    esc_attr($product_id),
                                    esc_attr($_product->get_sku())
                                ), $cart_item_key);


                            }


                            ?>

                        </td>

                        <td class="product-thumbnail cart_td">
                            <?php
                            $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

                            if ( ! $product_permalink ) {
                                echo $thumbnail;
                            } else {
                                printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
                            }
                            ?>
                        </td>

                        <td class="product-name  cart_td" data-title="<?php printf( __('Product', 'woo-rfq-for-woocommerce')); ?>">
                            <?php
                            if (!$product_permalink) {
                                echo apply_filters('woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key) . '&nbsp;';
                            } else {
                                echo apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s</a>', esc_url($product_permalink), $_product->get_title()), $cart_item, $cart_item_key);
                            }

                            // Meta data
                            rfq_cart_get_item_data($cart_item);

                            do_action('gplsrfq_cart_item_product',$_product, $cart_item, $cart_item_key);

                            do_action('gpls_woo_rfq_get_product_extra',$_product,$cart_item, $cart_item_key);



                            // Backorder notification
                            if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                                echo '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woo-rfq-for-woocommerce') . '</p>';
                            }
                            ?>
                        </td>



                        <td class="product-quantity  cart_td" data-title="<?php printf( __('Quantity', 'woo-rfq-for-woocommerce')); ?>">
                            <?php


                            if ($_product->is_sold_individually()) {
                                $product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
                            } else if ((isset($cart_item['bundled_by']) && isset($cart_item['bundled_by']))
                                || (isset($cart_item['bundled_items']) && isset($cart_item['bundled_items']) )
                            ) {
                                $product_quantity = sprintf("{$cart_item['quantity']} <input type='hidden' name='cart[%s][qty]' value='{$cart_item['quantity']}' />", $cart_item_key);
                            }else if ((isset($cart_item['composite_parent']) && isset($cart_item['composite_parent']))
                                || (isset($cart_item['composite_children']) && isset($cart_item['composite_children']) )
                            ) {
                                $product_quantity = sprintf("{$cart_item['quantity']} <input type='hidden' name='cart[%s][qty]' value='{$cart_item['quantity']}' />", $cart_item_key);
                            }

                            else {
                                $product_quantity = woocommerce_quantity_input(array(
                                    'input_name' => "cart[{$cart_item_key}][qty]",
                                    'input_value' => $cart_item['quantity'],
                                    'max_value' => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                                    'min_value' => '0',
                                    ''
                                ), $_product, false);
                            }
                    if ((isset($cart_item['bundled_items']) && isset($cart_item['bundled_items']) )) {
                        echo '<b style="padding-left: 1.3em">'.$product_quantity.'</b>';
                    }else if ((isset($cart_item['composite_children']) && isset($cart_item['composite_children']) )) {
                                echo '<b style="padding-left: 1.3em">'.$product_quantity.'</b>';
                    }
                    else{
                        echo $product_quantity;
                    }


                            ?>
                        </td>


                    </tr>
                    <?php
                }
            }
            ?>

            <?php

            do_action('gpls_woo_rfq_after_cart_contents');

            ?>

            <tr class="cart_tr">

                <td colspan="6" class="actions cart_td">

                    <?php

                   // $confirmation_message = get_option('rfq_cart_wordings_gpls_woo_rfq_update_rfq_cart_button',__('Update Quote Request', 'woo-rfq-for-woocommerce'));
                    $confirmation_message = __($confirmation_message,'woo-rfq-for-woocommerce');

                    ?>

                    <div class="update_rfq_cart">

                        <?php do_action('gpls_woo_rfq_cart_actions'); ?>

                        <input   type="submit" class="update-rfq-cart button alt gpls-woo-rfq_update-rfq-cart_button"
                                 id="update_rfq_cart" formnovalidate="formnovalidate"
                                 name="update_rfq_cart" value="<?php echo __($confirmation_message,'woo-rfq-for-woocommerce'); ?>"
                                 onmouseover="<?php echo $gpls_woo_rfq_styles['gpls_woo_rfq_page_update_button_onmouseover'].';'.$gpls_woo_rfq_styles['gpls_woo_rfq_page_update_button_background_onmouseover']; ?>"
                                 onmouseout="<?php echo $gpls_woo_rfq_styles['gpls_woo_rfq_page_update_button_onmouseout'].';'.$gpls_woo_rfq_styles['gpls_woo_rfq_page_update_button_background_onmouseout']; ?>"
                                 style = "margin-right: 1em;<?php echo $gpls_woo_rfq_styles['gpls_woo_rfq_page_update_button_styles'] ?>"
                        />
                    </div>
                </td>
            </tr> 
        </table>
        <table id="rfq-shop-table_customer_info" class="shop_table cart shop_table_responsive rfq-shop-table_customer_info rfq-custom-form" cellspacing="1"   cellpadding="1" >

            <?php

            do_action('gpls_woo_rfq_before_customer_info_label');

            ?>

            <tr class="info_tr">
                <?php
                $customer_info_label = get_option('settings_gpls_woo_rfq_customer_info_label','Checkout');
                $customer_info_label = __($customer_info_label,'woo-rfq-for-woocommerce');

                if(!isset($customer_info_label)){
                    $customer_info_label = __('Checkout','woo-rfq-for-woocommerce');
                }

                ?>


            </tr>

            <?php do_action('gpls_woo_rfq_cart_actions_customer_bid'); ?>

            <?php if (function_exists('wp_get_current_user')): ?>
            <?php if (!wp_get_current_user()->exists()): ?>
                <tr class="info_tr medium-width custom-rfq-field">

                    <th class="FName info_th"><?php printf( __('Fornavn', 'woo-rfq-for-woocommerce')); ?> <abbr class="required" required="required"></abbr>
                    </th>
					
                    <td class="info_td"><input style=" " type="text" id="rfq_fname" name="rfq_fname" placeholder=""
                                                /></td>
                </tr>
                <tr class="info_tr medium-width custom-rfq-field">
                    <th class="LName  info_th"><?php printf( __('Efternavn', 'woo-rfq-for-woocommerce')); ?> <abbr class="required" required="required"></abbr>
                   

                    <td class="info_td">
                        <input style=" " type="text" id="rfq_lname" name="rfq_lname" placeholder="" class="required"  /></td>

                </tr>
                <tr class="info_tr medium-width custom-rfq-field">

                    <th class="email  info_th"><?php printf( __('E-mail', 'woo-rfq-for-woocommerce')); ?> <abbr class="required" required="required"></abbr></th>

                    <td class="info_td"><input style=" " id="rfq_email_customer" name="rfq_email_customer" type="email"
                                               class="email required"  type="text"    /></td>
                </tr>


                <tr class="info_tr medium-width custom-rfq-field" >


                    <th class="info_th"><?php printf( __('Telefon', 'woo-rfq-for-woocommerce')); ?> <abbr  id="rfq_phone_label" ></abbr></th>

                    <td class="info_td"><input style="  !important" id="rfq_phone" name="rfq_phone" placeholder="" type="text"
                                                /></td>

                </tr>
                <tr class="info_tr medium-width custom-rfq-field">
                    <th colspan="2" class="company info_th"><?php printf( __('Firma', 'woo-rfq-for-woocommerce')); ?> <abbr  id="rfq_company_label"></abbr></th>


                    <td colspan="2" class="company info_td">
                        <input style="  " type="text" id="rfq_company" name="rfq_company" placeholder="" class="rfq_cart_address"
                                />
                    </td>

                </tr>

                <tr class="info_tr medium-width custom-rfq-field">
                    <th colspan="2" class="address info_td"><?php printf( __('Adresse', 'woo-rfq-for-woocommerce')); ?><abbr  id="rfq_address_label"></abbr></th>

                
                    <td colspan="2" class="address info_td">
                        <input style="  " type="text" id="rfq_address" name="rfq_address" placeholder="" class="rfq_cart_address" />
                    </td>

                </tr>

                <tr class="info_tr medium-width custom-rfq-field ">
                    <th colspan="2" class="address info_td info_th"><?php printf( __('Adresse 2', 'woo-rfq-for-woocommerce')); ?><abbr  id="rfq_address2_label"></abbr></th>

               
                    <td colspan="2" class="address info_td">
                        <input style="  " type="text" id="rfq_address2" name="rfq_address2" placeholder="Lejlighed, suite mv."
                               class="rfq_cart_address" />
                    </td>

                </tr>


                <tr class="info_tr medium-width custom-rfq-field">

                    <th class="info_th"><?php printf( __('By', 'woo-rfq-for-woocommerce')); ?><abbr  id="rfq_city_label"></abbr></th>

               
                    <td class="text info_td"><input style="  " class="form-control" type="text" id="rfq_city" name="rfq_city"
                                                                                               placeholder=""/></td>
                </tr>
                <tr class="info_tr medium-width custom-rfq-field">

                    <th class="info_th"><?php printf( __('Zip', 'woo-rfq-for-woocommerce')); ?><abbr  id="rfq_zip_label"></abbr></th>
 
                    <td class="info_td" >
                        <input style="  " class="form-control" name="rfq_zip" id="rfq_zip" type="text" placeholder=""
                                />
                    </td>

                </tr>



            <?php endif; ?>
            <?php endif; ?>

            <?php do_action('gpls_woo_rfq_cart_actions_ninja'); ?>

            <?php do_action('gpls_woo_rfq_cart_actions_upload_files'); ?>

            <?php if (function_exists('wp_get_current_user')): ?>
            <?php if (wp_get_current_user()->exists()): ?>
                <input type="hidden" id="rfq_fname" name="rfq_fname" value="rfq_fname"/>
                <input type="hidden" id="rfq_lname" name="rfq_lname" value="rfq_lname" />
                <input type="hidden" id="rfq_email_customer" name="rfq_email_customer" value="rfq_email_customer" />
            <?php endif; ?>
            <?php endif; ?>
            <tr class="info_tr medium-width custom-rfq-field">
                <th class="info_th"><?php printf( __('Kunde note', 'woo-rfq-for-woocommerce')); ?><abbr  id="rfq_message_label"></abbr></th>
                <td colspan="4"  style="" class="info_td">
                    <textarea id="rfq_message" name="rfq_message" placeholder="<?php printf( __('Din besked til os', 'woo-rfq-for-woocommerce')); ?>" rows="5" class="rfq-cart-message"   ></textarea>
                </td>
            </tr>
            <tr class="info_tr ful-width custom-rfq-field">
                <td>
                    <label class="terms-accept">Jeg accepterer handelsbetingelserne
                        <input required type="checkbox">
                        <span class="checkmark"></span>
                    </label>
                    <label class="apple-id">Mit Apple id er fjernet
                        <input required type="checkbox">
                        <span class="checkmark"></span>
                    </label>
                </td>
            </tr>

            <tr class="info_tr">
                <td colspan="2" align="center" class="info_td"  style="text-align: center !important;" >
                    <input type="hidden" name="gpls_woo_rfq_nonce" value='<?php echo $nonce; ?>'>
                    <?php
                    $button_text = get_option('rfq_cart_wordings_submit_your_rfq_text', 'Sælg din enhed');
                    $button_text = __($button_text,'woo-rfq-for-woocommerce');

                    $button_text = apply_filters('gpls_woo_rfq_rfq_submit_your_order_text',$button_text);

                    ?>

                    <div class="rfq_proceed-to-checkout" >
                        <input  name="gpls-woo-rfq_checkout_button"  type="submit" class="button alt gpls-woo-rfq_checkout_button"
                                style="<?php echo $gpls_woo_rfq_styles['gpls_woo_rfq_page_submit_button_styles'] ?>" value="<?php echo $button_text; ?>"
                                onmouseover="<?php echo $gpls_woo_rfq_styles['gpls_woo_rfq_page_submit_button_background_onmouseover'].';'.$gpls_woo_rfq_styles['gpls_woo_rfq_page_submit_button_onmouseover']; ?>"
                                onmouseout="<?php echo $gpls_woo_rfq_styles['gpls_woo_rfq_page_submit_button_onmouseout'].';'.$gpls_woo_rfq_styles['gpls_woo_rfq_page_submit_button_background_onmouseout']; ?>"

                        />
                        <br />
                        <input type="hidden" id="rfq_checkout" name="rfq_checkout" value="true"/>
                        <input type="hidden" id="gpls-woo-rfq_checkout" name="gpls-woo-rfq_checkout" value="false"/>
                        <input type="hidden" id="gpls-woo-rfq_update" name="gpls-woo-rfq_update" value="false"/>


                    </div>

                </td>

            </tr>


        </table>
        </div>

    <?php do_action('gpls_woo_woocommerce_after_cart_table'); ?>

</div>
    </form>
<div style="clear: both"></div>

<div class="woo_rfq_bottom_html_desc" >
    <?php
    do_action('gpls_woo_rfq_request_page_bottom_html_desc');

   // gpls_woo_rfq_cart_delete(gpls_woo_rfq_cart_tran_key() . '_' . 'gpls_woo_rfq_cart');



    ?>
</div>
<div id="terms-modal" class="modal custom-modal">
    <div class="modal-content">
        <span class="close">&times;</span>
        <p><strong>PRIVACY</strong></p>
        <p>Please review our Privacy Notice, which also governs your visit to our website, to understand our practices.</p>
        <p><strong>COPYRIGHT</strong></p>
        <p>All content included on this site, such as text, graphics, logos, button icons, images, audio clips, digital downloads, data compilations, and software, is the property of MYCOMPANY or its content suppliers and protected by international copyright laws.</p>
        <p><strong>QUESTIONS</strong></p>
        <p>Questions regarding our Conditions of Usage, Privacy Policy, or other policy related material can be directed to our support staff by clicking on the "Contact Us" link.</p>
    </div>
</div>
<div id="apple-id-modal" class="modal custom-modal">
    <div class="modal-content">
        <span class="close">&times;</span>
        <p>Mit Apple id er fjernet</p>
    </div>
</div>
</div>
<script type="text/javascript">
    jQuery('.terms-accept').on('click', function() {
        jQuery('#terms-modal.custom-modal').css('display', 'block')
    });
   jQuery('.apple-id').on('click', function() {
        jQuery('#apple-id-modal.custom-modal').css('display', 'block')
    });
    jQuery('.custom-modal .close').on('click', function() {
        jQuery('.custom-modal').css('display', 'none')
    });
</script>