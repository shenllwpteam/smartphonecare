<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product;
?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	do_action( 'woocommerce_before_single_product' );
	
	if ( post_password_required() ) {
		echo get_the_password_form();
		return;
	}
	
	
	// prev & next post -------------------
	$single_post_nav = array(
		'hide-header'	=> false,
		'hide-sticky'	=> false,
	);
	
	$opts_single_post_nav = mfn_opts_get( 'prev-next-nav' );
	if( is_array( $opts_single_post_nav ) ){
	
		if( isset( $opts_single_post_nav['hide-header'] ) ){
			$single_post_nav['hide-header'] = true;
		}
		if( isset( $opts_single_post_nav['hide-sticky'] ) ){
			$single_post_nav['hide-sticky'] = true;
		}
	
	}
	
	$post_prev = get_adjacent_post( false, '', true );
	$post_next = get_adjacent_post( false, '', false );
	
	// WC < 2.7 backward compatibility
	if( version_compare( WC_VERSION, '2.7', '<' ) ){
		$shop_page_id = woocommerce_get_page_id( 'shop' );
	} else {
		$shop_page_id = wc_get_page_id( 'shop' );
	}

	
	// post classes -----------------------
	$classes = array();
	
	if( mfn_opts_get( 'share' ) == 'hide-mobile' ){
		$classes[] = 'no-share-mobile';
	} elseif( ! mfn_opts_get( 'share' ) ) {
		$classes[] = 'no-share';
	}
	
	$single_product_style = mfn_opts_get( 'shop-product-style' );
	$classes[] = $single_product_style;
	 
	
	// translate
	$translate['all'] = mfn_opts_get('translate') ? mfn_opts_get('translate-all','Show all') : __('Show all','betheme');
	
	
	// WC < 2.7 backward compatibility
	if( version_compare( WC_VERSION, '2.7', '<' ) ){		
		$product_schema = 'itemscope itemtype="'. woocommerce_get_product_schema() .'"';
	} else {
		$product_schema = '';
	}
?>

<div <?php echo $product_schema; ?> id="product-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
	
	<?php 
		// single post navigation | sticky
		if( ! $single_post_nav['hide-sticky'] ){
			echo mfn_post_navigation_sticky( $post_prev, 'prev', 'icon-left-open-big' ); 
			echo mfn_post_navigation_sticky( $post_next, 'next', 'icon-right-open-big' );
		} 
	?>
		
	<?php 
		// single post navigation | header
		if( ! $single_post_nav['hide-header'] ){
			 mfn_post_navigation_header( $post_prev, $post_next, $shop_page_id, $translate );
		}
	?>

		<div class="sale-to animate fadeInLeftLarge" data-anim-type="fadeInLeftLarge" >
			<h1 class="single-product-title">VÆLG FARVE, STØRRELSE OG STAND</h1>
		    <div class="desc">			       
		       <?php do_action( 'woocommerce_single_product_summary' ); ?>
		    </div>
		    <div class="image_wrapper animate fadeInRightLarge" data-anim-type="fadeInRightLarge">		       
		        <?php 
		        	//remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
				    $attributes = array(
					'title'                   	=> get_post_field( 'post_title', $post_thumbnail_id ),
					'data-caption'            	=> get_post_field( 'post_excerpt', $post_thumbnail_id ),
					'data-src'                	=> $full_size_image[0],
					'data-large_image'        	=> $full_size_image[0],
					'data-large_image_width'  	=> $full_size_image[1],
					'data-large_image_height'	=> $full_size_image[2],
				);
		
				if ( has_post_thumbnail() ) {
					$html  = '<div data-thumb="' . get_the_post_thumbnail_url( $post->ID, 'shop_thumbnail' ) . '" class="woocommerce-product-gallery__image"><a href="' . esc_url( $full_size_image[0] ) . '">';
					$html .= get_the_post_thumbnail( $post->ID, 'shop_single', $attributes );
					$html .= '</a></div>';
				} else {
					$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
					$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src() ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
					$html .= '</div>';
				}
				
				echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, get_post_thumbnail_id( $post->ID ) );
				?>
		    </div>
		    <h2 class="title"><?php //the_title(); ?></h2> 
		    <div class="sale-detail animate zoomIn " data-anim-type="zoomIn">
		    	<p>Du kan Få: <span class="price"></span> <span>DKK</span></p>
		    </div>
		</div>
		<div class="summary entry-summary column one-second">
			<?php				
				// Description | Default - right column
				if( in_array( $single_product_style, array( 'wide', 'wide tabs') ) ) {
					remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
				}
				//do_action( 'woocommerce_before_single_product_summary' );	
			?>
	
		</div>
	
	<?php 
		// Description | Default - wide below image
		if( in_array( $single_product_style, array( 'wide', 'wide tabs') ) ) {
			woocommerce_output_product_data_tabs(); 
		}
	?>
	
	<?php if( version_compare( WC_VERSION, '2.7', '<' ) ): ?>
		<meta itemprop="url" content="<?php the_permalink(); ?>" />
	<?php endif; ?>


</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
