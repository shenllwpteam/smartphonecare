<?php
/* Write your awesome functions below */


Class SmartphoneRequestQuotes {

	public $plugin_dir;
	public $plugin_url;

	function  __construct(){
		global $wpdb;
		$prefix = $wpdb->prefix;
		$this->plugin_dir = plugin_dir_path(__FILE__);
		$this->plugin_url = plugin_dir_url(__FILE__);	

		add_filter( 'wc_product_sku_enabled', array($this, 'sv_remove_product_page_skus' ));
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
		add_filter( 'woocommerce_get_stock_html', '__return_empty_string' );
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );		
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
		add_filter( 'woocommerce_is_sold_individually', array($this, 'woo_remove_all_quantity_fields'), 10, 2 );
		add_action( 'woocommerce_after_single_product', array($this,'clone_rfq_button'), 20 );
		add_filter( 'woocommerce_subcategory_count_html', array($this,'woo_remove_category_products_count') );
		add_action('admin_head',  array($this,'admin_js'));
		add_filter( 'woocommerce_add_to_cart_redirect', array($this, 'my_custom_add_to_cart_redirect' ));
	}

	public function sv_remove_product_page_skus( $enabled ) {
		if ( ! is_admin() && is_product() ) {
			return false;
		}
		return $enabled;
	}

	public function woo_remove_category_products_count() {
		return;
	}

	public function admin_js() { ?>
		<script type="text/javascript">
			jQuery(document).ready( function () {
				jQuery('form#post').find('.categorychecklist input').each(function() {
					jQuery(this).prop("type","radio");
				});
			});
		</script>
	<?php }


	public function clone_rfq_button() { ?>
		<script type="text/javascript">
			jQuery(document).ready( function () {
				jQuery('.gpls_rfq_set_div button').clone().appendTo('.sale-detail');
				jQuery('.sale-detail button').html('Sælg din enhed');
				jQuery("html, body").animate({ scrollTop: 0 });
				jQuery('.sale-detail button.single_add_to_cart_button').on('click', function() {
					jQuery('.gpls_rfq_set_div button').click();
				});
				jQuery('.variations_form').each( function() {
					jQuery(this).on('found_variation', function( event, variation ) {
						jQuery('.sale-detail p .price').html(variation.display_price);
						jQuery('.sale-detail p').slideDown();
					});
				});
				jQuery('.variations_form select').blur( function(){
					if( '' == jQuery('input.variation_id').val() ){
						jQuery('.sale-detail p').slideUp();
					}
				});
				jQuery('.alg-wc-civs-attribute.color').append('<ul class="color-dropdown"><li class="init">Vælg mulighed</li></ul>');
				jQuery('.alg-wc-civs-attribute.color span').each( function() {
					var color = jQuery(this).attr('data-value');
					jQuery('.alg-wc-civs-attribute.color ul.color-dropdown').append('<li data-attribute="pa_farve" data-value='+color+'><span style="background-color: '+color+'"></span>'+color+'</li>');
				});
				jQuery("ul.color-dropdown").on("click", ".init", function() {
					jQuery(this).closest("ul.color-dropdown").children('li:not(.init)').toggle();
				});
				var allOptions = jQuery("ul.color-dropdown").children('li:not(.init)');
				jQuery("ul.color-dropdown").on("click", "li:not(.init)", function() {
					allOptions.removeClass('selected');
					jQuery(this).addClass('selected');
					jQuery("ul").children('.init').html(jQuery(this).html());
					allOptions.toggle();
				});
				jQuery('ul.color-dropdown li').on('click', function() {
					var color = jQuery(this).attr('data-value');
					jQuery('.alg-wc-civs-attribute.color span.color[data-value="'+ color +'"]').click();
				});
				jQuery("body").click(function(e){
					if(e.target.className !== "color-dropdown" && e.target.className !== "init") {
						jQuery(".color-dropdown li:not(.init)").hide();
					}
				});
			});
		</script>
	<?php }


	public function woo_remove_all_quantity_fields( $return, $product ) { 
		return true; 
	} 

	public function my_custom_add_to_cart_redirect( $url ) {
		$url  = get_option("rfq_cart_sc_section_show_link_to_rfq_page");
		return $url;
	}
	
}


$SmartphoneRequestQuotes = new SmartphoneRequestQuotes();

add_action( 'wp_enqueue_scripts', 'wsis_dequeue_stylesandscripts_select2', 100 );

function wsis_dequeue_stylesandscripts_select2() {
    if ( class_exists( 'woocommerce' ) ) {       

        wp_enqueue_script( 'select2');
        //wp_register_script('select2', '//cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.js', false, '1.0', 'all' );
        wp_register_style( 'select2css', '//cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.css', false, '1.0', 'all' );   
		wp_enqueue_style( 'select2css' );
    } 
} 


function wc_dropdown_variation_attribute_options( $args = array() ) {
    $args = wp_parse_args( apply_filters( 'woocommerce_dropdown_variation_attribute_options_args', $args ), array(
      'options'          => false,
      'attribute'        => false,
      'product'          => false,
      'selected'          => false,
      'name'             => '',
      'id'               => '',
      'class'            => '',
      'show_option_none' => __( 'Vælg mulighed', 'woocommerce' ),
    ) );

    $options               = $args['options'];
    $product               = $args['product'];
    $attribute             = $args['attribute'];
    $name                  = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title( $attribute );
    $id                    = $args['id'] ? $args['id'] : sanitize_title( $attribute );
    $class                 = $args['class'];
    $show_option_none      = $args['show_option_none'] ? true : false;
    $show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __( 'Vælg mulighed', 'woocommerce' ); // We'll do our best to hide the placeholder, but we'll need to show something when resetting options.

    if ( empty( $options ) && ! empty( $product ) && ! empty( $attribute ) ) {
      $attributes = $product->get_variation_attributes();
      $options    = $attributes[ $attribute ];
    }

    $html = '<div class="selectdiv"><select id="' . esc_attr( $id ) . '" class="' . esc_attr( $class ) . '" name="' . esc_attr( $name ) . '" data-attribute_name="attribute_' . esc_attr( sanitize_title( $attribute ) ) . '" data-show_option_none="' . ( $show_option_none ? 'yes' : 'no' ) . '">';
    $html .= '<option value="">' . esc_html( $show_option_none_text ) . '</option>';

    if ( ! empty( $options ) ) {
      if ( $product && taxonomy_exists( $attribute ) ) {
        // Get terms if this is a taxonomy - ordered. We need the names too.
        $terms = wc_get_product_terms( $product->get_id(), $attribute, array( 'fields' => 'all' ) );

        foreach ( $terms as $term ) {
          if ( in_array( $term->slug, $options ) ) {
            $html .= '<option value="' . esc_attr( $term->slug ) . '" ' . selected( sanitize_title( $args['selected'] ), $term->slug, false ) . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name ) ) . '</option>';
          }
        }
      } else {
        foreach ( $options as $option ) {
          // This handles < 2.4.0 bw compatibility where text attributes were not sanitized.
          $selected = sanitize_title( $args['selected'] ) === $args['selected'] ? selected( $args['selected'], sanitize_title( $option ), false ) : selected( $args['selected'], $option, false );
          $html .= '<option value="' . esc_attr( $option ) . '" ' . $selected . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $option ) ) . '</option>';
        }
      }
    }

    $html .= '</select></div>';

    echo apply_filters( 'woocommerce_dropdown_variation_attribute_options_html', $html, $args ); // WPCS: XSS ok.
  }
